package srio_txrx_txdev16_functions;
import srio_txrx_dtypes ::*;
import srio_txrx_ftypes ::*;

//Ftype 2 NREAD
function Data fn_dev16_ftype2_header (Ftype2_read_request ftype_pkt, Bit#(16) dest_id, Bit#(16)
source_id, Bit#(2) tt);

  case (ftype_pkt.ttype) matches
      'b0100: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0],
              ftype_pkt.wdptr, ftype_pkt.xamsbs,16'h00};

      'b1100: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0],
              ftype_pkt.wdptr, ftype_pkt.xamsbs,16'h00};
      
      'b1101: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0],
              ftype_pkt.wdptr, ftype_pkt.xamsbs,16'h00};

      'b1110: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0],
              ftype_pkt.wdptr, ftype_pkt.xamsbs,16'h00};

      'b1111: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0],
              ftype_pkt.wdptr, ftype_pkt.xamsbs,16'h00};

    default : return 0;
  endcase
endfunction

endpackage : srio_txrx_txdev16_functions 
