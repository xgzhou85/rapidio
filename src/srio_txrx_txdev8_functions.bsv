package srio_txrx_txdev8_functions;
import srio_txrx_dtypes ::*;
import srio_txrx_ftypes ::*;

//Ftype 2 NREAD
function Data fn_dev8_ftype2_header (Ftype2_read_request ftype_pkt, Bit#(8) dest_id, Bit#(8)
source_id, Bit#(2) tt);

  case (ftype_pkt.ttype) matches
      'b0100: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0], 
              ftype_pkt.wdptr, ftype_pkt.xamsbs,32'h00};

      'b1100: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0],
              ftype_pkt.wdptr, ftype_pkt.xamsbs,32'h00};
      
      'b1101: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0],
              ftype_pkt.wdptr, ftype_pkt.xamsbs,32'h00};

      'b1110: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0],
              ftype_pkt.wdptr, ftype_pkt.xamsbs,32'h00};

      'b1111: return {8'h00, ftype_pkt.prio, tt, ftype_pkt.ftype, dest_id, source_id,
              ftype_pkt.ttype, ftype_pkt.rdsize, ftype_pkt.srctid, ftype_pkt.addr[44:0],
              ftype_pkt.wdptr, ftype_pkt.xamsbs,32'h00};

    default : return 0;
  endcase
endfunction
   
// Ftype 5 NWRITE
function Data fn_dev8_ftype5_header(Ftype5_write_request ftype_pkt, Bit#(8) dest_id, Bit#(8)
source_id, Prio prio, Bit#(2) tt); 
  
  case (ftype_pkt.ttype) matches
      'b0100: return {8'h00, prio, tt, ftype_pkt.ftype, dest_id, source_id, ftype_pkt.ttype,
      ftype_pkt.wrsize, ftype_pkt.srctid, ftype_pkt.addr[44:0], ftype_pkt.wdptr,
      ftype_pkt.xamsbs, 32'h00};

      'b0101: return {8'h00, prio, tt, ftype_pkt.ftype, dest_id, source_id, ftype_pkt.ttype,
      ftype_pkt.wrsize, ftype_pkt.srctid, ftype_pkt.addr[44:0], ftype_pkt.wdptr, ftype_pkt.xamsbs,
      32'h00};
// Atomic transactions to be implemented
    default : return 0;
  endcase
endfunction

// Ftype 6 SWRITE
function Data fn_dev8_ftype6_header(Ftype6_streaming_write ftype_pkt, Bit#(2) prio, Bit#(2) tt,
Bit#(8) destId, Bit#(8) sourceId, Bit#(1) resv);
// Will be extended with data
  if (ftype_pkt.ftype == 'b0110)
    return {8'h00, prio, tt, ftype_pkt.ftype, destId, sourceId, ftype_pkt.addr, resv, 
    ftype_pkt.xamsbs, 48'h00};
  else
    return 0;
   
endfunction




endpackage : srio_txrx_txdev8_functions 


