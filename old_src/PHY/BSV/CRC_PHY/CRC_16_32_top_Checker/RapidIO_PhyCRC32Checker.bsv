/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
-- Description
-- This Module is to check the CRC32 code developed for the input data.
-- 1.Design is implemented as mentioned in the RapidIO specification 3.1. 
-- 2.Function is developed to perform the CRC-32 checker (same function as used for generation in the specification). 
-- 3.Input to the data includes data appended with generated CRC16, zero padding if necessary and generated CRC32.
--
-- To be done
-- 1.Crf yet to be implemented.
-- 2.Parameteristion of data width in each cycle and also width of rem value to be done later.
-- 
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_PhyCRC32Checker;

`define RIO_DATA 128
`define RIO_DATA_REM 3

// The Endian format is changed from Big Endian to little Endian
// Function is used to perform three level operation of CRC-32.
function Bit#(32) fn_CRC32Generation(Bit#(32) old_check_symbol, Bit#(32) data_in);

// 1st Level Operation (XORing Data and Old Symbol)
	Bit#(32) lv_IntermediateSymbol = data_in ^ old_check_symbol;

// 2nd Level Operation (Equation Network)
	Bit#(1) lv_Check_Symbol_0 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[30] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_1 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[28];

    	Bit#(1) lv_Check_Symbol_2 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[30] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_3 = lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_4 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[30] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_5 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[29];
 
    	Bit#(1) lv_Check_Symbol_6 = lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[30];

    	Bit#(1) lv_Check_Symbol_7 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[29];

    	Bit#(1) lv_Check_Symbol_8 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_9 = lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[29];

    	Bit#(1) lv_Check_Symbol_10 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_11 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_12 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[30] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_13 = lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_14 = lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[29];

    	Bit#(1) lv_Check_Symbol_15 = lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[30];

    	Bit#(1) lv_Check_Symbol_16 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[30];

    	Bit#(1) lv_Check_Symbol_17 = lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[30] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_18 = lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_19 = lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[29];

    	Bit#(1) lv_Check_Symbol_20 = lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[30];

    	Bit#(1) lv_Check_Symbol_21 = lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_22 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_23 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_24 = lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[16] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[30];

    	Bit#(1) lv_Check_Symbol_25 = lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[17] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_26 = lv_IntermediateSymbol[0] ^ lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[18] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_27 = lv_IntermediateSymbol[1] ^ lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[19] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[29];

    	Bit#(1) lv_Check_Symbol_28 = lv_IntermediateSymbol[2] ^ lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[12] ^ lv_IntermediateSymbol[20] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[30];

    	Bit#(1) lv_Check_Symbol_29 = lv_IntermediateSymbol[3] ^ lv_IntermediateSymbol[6] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[13] ^ lv_IntermediateSymbol[21] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[31];

    	Bit#(1) lv_Check_Symbol_30 = lv_IntermediateSymbol[4] ^ lv_IntermediateSymbol[7] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[10] ^ lv_IntermediateSymbol[14] ^ lv_IntermediateSymbol[22] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[26] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[30];

    	Bit#(1) lv_Check_Symbol_31 = lv_IntermediateSymbol[5] ^ lv_IntermediateSymbol[8] ^ lv_IntermediateSymbol[9] ^ lv_IntermediateSymbol[11] ^ lv_IntermediateSymbol[15] ^ lv_IntermediateSymbol[23] ^ lv_IntermediateSymbol[24] ^ lv_IntermediateSymbol[25] ^ lv_IntermediateSymbol[27] ^ lv_IntermediateSymbol[28] ^ lv_IntermediateSymbol[29] ^ lv_IntermediateSymbol[30] ^ lv_IntermediateSymbol[31];

// 3rd Level Operation - Generates Next Check Symbol 
    	return{lv_Check_Symbol_31,lv_Check_Symbol_30,lv_Check_Symbol_29,lv_Check_Symbol_28,lv_Check_Symbol_27,lv_Check_Symbol_26,          lv_Check_Symbol_25,lv_Check_Symbol_24,lv_Check_Symbol_23,lv_Check_Symbol_22,lv_Check_Symbol_21,lv_Check_Symbol_20,           lv_Check_Symbol_19,lv_Check_Symbol_18,lv_Check_Symbol_17,lv_Check_Symbol_16,lv_Check_Symbol_15,lv_Check_Symbol_14,           lv_Check_Symbol_13,lv_Check_Symbol_12,lv_Check_Symbol_11,lv_Check_Symbol_10,lv_Check_Symbol_9,lv_Check_Symbol_8,           lv_Check_Symbol_7,lv_Check_Symbol_6,lv_Check_Symbol_5,lv_Check_Symbol_4,lv_Check_Symbol_3,lv_Check_Symbol_2,lv_Check_Symbol_1,lv_Check_Symbol_0};

endfunction


interface Ifc_RapidIO_PhyCRC32Checker;

//input methods and output methods
	method Action _link_rx_sof_n (Bool value);
	method Action _link_rx_eof_n (Bool value);
	method Action _link_rx_vld_n (Bool value);
	method Action _link_rx_rem (Bit#(`RIO_DATA_REM) value);
	method Action _link_rx_data (Bit#(`RIO_DATA) value);
	//method Action _link_tx_crf (Bit#(2) value); //yet to be implemented
  
	method Bit#(32) output_CRC32_check_();//output crc 32 checksum

endinterface : Ifc_RapidIO_PhyCRC32Checker


(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIO_PhyCRC32Checker(Ifc_RapidIO_PhyCRC32Checker);

//input signals are given as wires
Wire#(Bool) wr_rx_sof <- mkDWire (True);
Wire#(Bool) wr_rx_eof <- mkDWire (True);
Wire#(Bool) wr_rx_vld <- mkDWire (True);
Wire#(Bit#(`RIO_DATA_REM)) wr_rx_rem <- mkDWire (0);
Wire#(Bit#(`RIO_DATA)) wr_rx_data <- mkDWire (0);
//Wire#(Bool) wr_tx_crf <- mkDWire (True);

//Internal Wires
Wire#(Bit#(32)) wr_CheckSymbolChk_0 <- mkDWire (0);
Wire#(Bit#(32)) wr_CheckSymbolChk_1 <- mkDWire (0);
Wire#(Bit#(32)) wr_CheckSymbolChk_2 <- mkDWire (0);
Wire#(Bit#(32)) wr_CheckSymbolChk_3 <- mkDWire (0);

//Internal register
Reg#(Bit#(32)) rg_OldCheckSymbol <- mkReg (0);//for storing intermediate crc value in case input data is more than a single cycle of data.i.e., more than 128 bits

/*
-- CRC check value is held in a wire for every stage.
-- Final Stage of the CRC check value is stored in  the Register. 
*/

//Checking operation using corresponding bits for each tx_rem value
rule rl_CRC_32_Checker(wr_rx_vld == False);

	Bit#(32) lv_OldCheckSymbol = (wr_rx_sof == False) ? 32'hffffffff : rg_OldCheckSymbol;
//To set 6 bit ACK ID bits to 0
	Bit#(32) lv_InitialData = (wr_rx_sof == False) ? {6'h00, wr_rx_data[(`RIO_DATA-7):(`RIO_DATA-32)]} : wr_rx_data[(`RIO_DATA-1):(`RIO_DATA-32)];


//tx_rem = 101 & tx_rem = 110
	Bit#(32) lv_CheckSymbolChk_3 = fn_CRC32Generation (lv_OldCheckSymbol, wr_rx_data[(`RIO_DATA-1):(`RIO_DATA-32)]);
//crc32 code appended at [127:96]
	wr_CheckSymbolChk_3  <= fn_CRC32Generation (lv_OldCheckSymbol, lv_InitialData);


//tx_rem = 000 & tx_rem = 111
	Bit#(32) lv_CheckSymbolChk_0_1 = fn_CRC32Generation (lv_OldCheckSymbol,lv_InitialData);
	Bit#(32) lv_CheckSymbolChk_0 = fn_CRC32Generation (lv_CheckSymbolChk_0_1, wr_rx_data[(`RIO_DATA-33):(`RIO_DATA-64)]);
//crc32 code appended at [95:64]
	wr_CheckSymbolChk_0  <= fn_CRC32Generation (lv_CheckSymbolChk_3, wr_rx_data[(`RIO_DATA-33):(`RIO_DATA-64)]);


//tx_rem = 001 & tx_rem = 010
	Bit#(32) lv_CheckSymbolChk_1 = fn_CRC32Generation (lv_CheckSymbolChk_0, wr_rx_data[(`RIO_DATA-65):(`RIO_DATA-96)]);
//crc32 code appended at [63:32]
         wr_CheckSymbolChk_1 <= fn_CRC32Generation (lv_CheckSymbolChk_0, wr_rx_data[(`RIO_DATA-65):(`RIO_DATA-96)]);


//tx_rem = 011 & tx_rem = 100
	Bit#(32) lv_CheckSymbolChk_2 = fn_CRC32Generation (lv_CheckSymbolChk_1, wr_rx_data[(`RIO_DATA-97):(`RIO_DATA-`RIO_DATA)]);
//crc32 code appended at [31:0]
         wr_CheckSymbolChk_2 <= fn_CRC32Generation (lv_CheckSymbolChk_1, wr_rx_data[(`RIO_DATA-97):(`RIO_DATA-`RIO_DATA)]);

//stores intermediate crc value
	rg_OldCheckSymbol <= fn_CRC32Generation (lv_CheckSymbolChk_1, wr_rx_data[(`RIO_DATA-97):(`RIO_DATA-`RIO_DATA)]);

endrule 


//Input and output method definitions

method Action _link_rx_sof_n (Bool value);
	wr_rx_sof <= value;
endmethod 

method Action _link_rx_eof_n (Bool value);
	wr_rx_eof <= value;
endmethod 

method Action _link_rx_vld_n (Bool value);
	wr_rx_vld <= value; 
endmethod 

method Action _link_rx_data (Bit#(`RIO_DATA) value);
	wr_rx_data <= value;
endmethod 

method Action _link_rx_rem (Bit#(`RIO_DATA_REM) value);
	wr_rx_rem <= value; 
endmethod 

/*method Action _link_tx_crf (Bit#(2) value);
	wr_tx_crf <= value; 
endmethod */

method Bit#(32) output_CRC32_check_();//output crc checksum returning method
	if (wr_rx_eof == False) 
		begin
		if (wr_rx_rem == 3'b000 || wr_rx_rem == 3'b111)
			return wr_CheckSymbolChk_0;
		else if (wr_rx_rem == 3'b001 || wr_rx_rem == 3'b010)
			return wr_CheckSymbolChk_1;
		else if (wr_rx_rem == 3'b011 || wr_rx_rem == 3'b100)
			return wr_CheckSymbolChk_2;
		else 
			return wr_CheckSymbolChk_3;
		end 
	else 
		return 0; 
endmethod 


endmodule:mkRapidIO_PhyCRC32Checker
endpackage:RapidIO_PhyCRC32Checker
