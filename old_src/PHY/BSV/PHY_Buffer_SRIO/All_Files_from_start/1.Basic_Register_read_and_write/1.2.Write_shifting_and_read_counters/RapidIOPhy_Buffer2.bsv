package RapidIOPhy_Buffer2;

import RapidIO_DTypes ::*;
import AckId_generator ::*;

interface Ifc_RapidIOPhy_Buffer2;
   method Action _tx_sof_n(Bool value);
   method Action _tx_eof_n(Bool value);
   method Action _tx_vld_n(Bool value);
   method Action _tx_data(DataPkt value);
   method Action _tx_rem(Bit#(4) value);
   method Action _tx_crf_n(Bit#(2) value);
   method Action _tx_deq(Bit#(1) value);
   method Action _tx_read(Bit#(1) value);
   method Action _tx_ack(Bit#(6) value);
   
   method Bool lnk_tsof_n_();
   method Bool lnk_teof_n_();
   method DataPkt lnk_td_();
   method Bool lnk_tvld_n_();
   method Bit#(4) lnk_trem_();
   method Bit#(2) lnk_tcrf_();
   method Bit#(1) read_eof_();
   method RegBuf buf_out_();



endinterface:Ifc_RapidIOPhy_Buffer2



(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer2(Ifc_RapidIOPhy_Buffer2);

Wire#(Bool) tx_sof_n <- mkDWire(True);
Wire#(Bool) tx_eof_n <- mkDWire(True);
Wire#(Bool) tx_vld_n <- mkDWire(True);
Wire#(DataPkt) tx_data <- mkDWire(0);
//Wire#(DataPkt) tx_last_data <- mkDWire(0);
Wire#(Bit#(4)) tx_rem <- mkDWire(0);
Wire#(Bit#(2)) tx_crf <- mkDWire(0);

Wire#(Bit#(2)) lnk_tcrf <- mkDWire(0);
Wire#(Bool) lnk_tsof_n <- mkDWire(True);
Wire#(Bool) lnk_teof_n <- mkDWire(True);
Wire#(Bool) lnk_tvld_n <- mkDWire(True);
Wire#(DataPkt) lnk_td <- mkDWire(0);
Wire#(Bit#(4)) lnk_trem <- mkDWire(0);

Wire#(Bit#(6)) tx_ack <- mkDWire(0);
Reg#(RegBuf) rg_buf <- mkReg(0);
Reg#(Bit#(1)) rg_read <- mkReg(0);// reg for write to read ie making both of them mutually exclusive 
Reg#(Bit#(5)) rg_cnt_rd <- mkReg(0);
Reg#(Bit#(5)) rg_cnt_wr <- mkReg(0);//for storing rg_cnt value
Wire#(Bit#(1)) wr_deq <- mkDWire(0);//reg for deciding about dequeue.            //total register size = 2312 bits
Wire#(Bit#(1)) eof <- mkDWire(0);//reg for indicating eof.
//Reg#(Bit#(5)) rg_cnt <- mkReg(0);
//Reg#(Bit#(12)) rg_pos <- mkReg(0);
//rule r1;
//a <= rg_cnt;
//rg_cnt <= rg_cnt + 128;
//endrule

rule r1_write(tx_vld_n == False || wr_deq == 1);                            ///writing into register
//Bit#(12) a ;
  
 	if(wr_deq == 1)
		rg_buf[2311:0] <= 2312'b0;
	else if(tx_sof_n == False && tx_eof_n == False)
		begin            
		rg_buf[2311:0] <= {1'b1,tx_crf,tx_rem,1'b1,2176'b0,tx_data};  //only one cycle(128 bits) of data; [2311:2304] stores eof crf rem and sof in 8 bits
		rg_cnt_wr <= rg_cnt_wr +1;
		end

	else if(tx_sof_n == False)
		begin            
		rg_buf[127:0] <= tx_data;                                      //first cycle always goes to position [127:0]
		rg_cnt_wr <= rg_cnt_wr +1;
//  rg_pos <= rg_pos + 128;
		end

	else if(tx_eof_n == False)
		begin
		let lv_rg_intrmediate = rg_buf[2175:128];
		rg_buf[(2311):(128)] <= {1'b1,tx_crf,tx_rem,1'b1,tx_data,lv_rg_intrmediate};     //last cycle always goes to position [2303:2176]; [2311:2304] stores eof crf rem and sof in 8 bits
//tx_last_data <=tx_data;  
		end 

	else 
		begin
//Bit#(12) b = a+127;
//Bit#(12) lv_pos = rg_pos;
//numeric type lv_pos = rg_pos;
//Integer lv_pos = pack(rg_pos);
//let lv_posn = valueOf(lv_pos1);
		let lv_rg_shift_in_data = rg_buf[2175:128];
		lv_rg_shift_in_data = lv_rg_shift_in_data >> 128;                                 //data is first written to msb of local register and then shifted to right whenever next 128 bits come. 
		lv_rg_shift_in_data[2047:1920]= lv_rg_shift_in_data[2047:1920] | tx_data;   //data is first written to msb of local register(local register is of 2048 bits excluding initial 8 bits and storage area of first cycle and last cycle of data.)
		rg_buf[2175:128]<= lv_rg_shift_in_data;
//rg_buf[(lv_pos+127):lv_pos] <= tx_data;
//rg_pos <= rg_pos + 128;
		rg_cnt_wr <= rg_cnt_wr +1;
		end

      

endrule






rule r1_read(rg_read == 1);


	if(rg_cnt_wr <= 1 && rg_cnt_rd == 0)
		begin
		if(rg_cnt_wr == 1)
			begin
			rg_buf[5:0] <= tx_ack[5:0];
			$display("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			rg_cnt_wr <= rg_cnt_wr-1;
			end
		else if(rg_cnt_wr == 0)
			begin
			lnk_tsof_n <= False;
			lnk_teof_n <= False;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[127:0];
			lnk_trem <= rg_buf[2308:2305];
			lnk_tcrf <= rg_buf[2310:2309];
			eof <= 1'b1;
			end
		end


	else if(rg_cnt_wr >= 1)

		begin
		if(rg_cnt_rd == 0)
			begin
			rg_buf[5:0] <= tx_ack[5:0];
			$display("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			rg_cnt_wr <= rg_cnt_wr-1;
			rg_cnt_rd <= rg_cnt_rd + 1;
			end



		else if(rg_cnt_rd == 1)
			begin
			lnk_tsof_n <= False;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[127:0];
			rg_cnt_wr <= rg_cnt_wr-1;
			rg_cnt_rd <= rg_cnt_rd + 1;
			//lnk_trem <= rg_buf[2308:2305];
			//lnk_tcrf <= rg_buf[2310:2309];
			//eof <= 1'b1;
			end
		else if(rg_cnt_rd == 2)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[255:128];
			rg_cnt_rd <= rg_cnt_rd + 1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 3)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[383:256];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 4)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[511:384];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 5)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[639:512];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 6)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[767:640];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 7)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[895:768];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 8)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[1023:896];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 9)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[1151:1024];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 10)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[1279:1152];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 11)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[1407:1280];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 12)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[1535:1408] ;
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 13)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[1663:1536];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 14)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[1791:1664];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 15)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[1919:1792];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 16)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[2047:1920];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end

		else if(rg_cnt_rd == 17)
			begin
			lnk_tsof_n <= True;
			lnk_teof_n <= True;
			lnk_tvld_n <= False;
			lnk_td <= rg_buf[2175:2048];
			rg_cnt_rd <= rg_cnt_rd+1;
			rg_cnt_wr <= rg_cnt_wr - 1;
			end



		end


	else if(rg_cnt_wr == 0 && rg_cnt_rd != 0)
		begin
		lnk_teof_n <= False;
		lnk_tsof_n <= True;
		lnk_tvld_n <= False;
		lnk_td <= rg_buf[2303:2176];
//rg_buf[2311] <= 1'b0;
		eof <= 1'b1;
		lnk_trem <= rg_buf[2308:2305];
		lnk_tcrf <= rg_buf[2310:2309];
		rg_cnt_rd <= 0;
//rg_read <= 1'b0;
//out[2311:0] <= rg_buf[2311:0];
//$display("Reg value is %b",out[2311:0]);
		end
		
endrule
/*
rule r1_read(rg_buf[2245] == 1'b1);

//Bit#(8) a = rg_cnt;
if(rg_buf[2240] == 1'b1)
begin
lnk_tsof_n <= False;
lnk_td <= rg_buf[127:0];
rg_buf[2240] <= 1'b0;
rg_cnt <= rg_cnt - 1;
a <= 128;
end

else
begin
lnk_td <= rg_buf[(a+127):a];
a <= a+128;
rg_cnt <= rg_cnt - 1;
end

if(rg_cnt == 1)
begin
lnk_teof_n <= False;
rg_buf[2245] <= 1'b0;
lnk_trem <= rg_buf[2244:2241];
end

endrule
*/
method Action _tx_ack(Bit#(6) value);
	tx_ack <= value;
endmethod

method Action _tx_sof_n(Bool value);
     tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     tx_eof_n <= value;
endmethod

method Action _tx_crf_n(Bit#(2) value);
     tx_crf <= value;
endmethod

method Action _tx_vld_n(Bool value);
     tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     tx_rem <= value;
endmethod

method Action _tx_deq(Bit#(1) value);
     wr_deq <= value;
endmethod

method Action _tx_read(Bit#(1) value);
     rg_read <= value;
endmethod



method Bool lnk_tsof_n_();
     return lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return lnk_trem;
endmethod

method Bit#(2) lnk_tcrf_();
      return lnk_tcrf;
endmethod

method Bool lnk_tvld_n_();
     return lnk_tvld_n;
endmethod

method Bit#(1) read_eof_();
	return eof;
endmethod

method RegBuf buf_out_();
      return rg_buf;
endmethod

endmodule:mkRapidIOPhy_Buffer2
endpackage:RapidIOPhy_Buffer2
