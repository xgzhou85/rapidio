package Tb_AckId_Lut;

import AckId_Lut::*;

(*synthesize*)
(*always_enabled*)
(*always_ready*)

module mkTb_AckId_Lut(Empty);

Ifc_AckId_Lut lut <- mkAckId_Lut;


Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 10 )
		$finish (0);
endrule


rule r1(reg_ref_clk == 1);
lut._identify(2'b01);
lut._ackid(6'b000000);
lut._rd_ptr_in(4'b0001);
lut._prio_in(2'b00);
endrule

rule r2(reg_ref_clk == 2);
lut._identify(2'b01);
lut._ackid(6'b000001);
lut._rd_ptr_in(4'b0010);
lut._prio_in(2'b01);
endrule

rule r3(reg_ref_clk == 3);
lut._identify(2'b01);
lut._ackid(6'b000010);
lut._rd_ptr_in(4'b0010);
lut._prio_in(2'b10);
endrule

rule r4(reg_ref_clk == 4);
lut._identify(2'b01);
lut._ackid(6'b000011);
lut._rd_ptr_in(4'b0010);
lut._prio_in(2'b11);
endrule

rule r5(reg_ref_clk == 5);
lut._identify(2'b01);
lut._ackid(6'b000100);
lut._rd_ptr_in(4'b0011);
endrule

rule r6(reg_ref_clk == 6);
lut._identify(2'b10);
lut._ackid(6'b000011);
endrule

rule r7;
//lut._identify(2'b10);
//lut._ackid(6'b000011);
$display("rd_ptr = %b",lut.rd_ptr_out_());
$display("buffer_prio = %b",lut.prio_out_());
endrule


endmodule:mkTb_AckId_Lut
endpackage:Tb_AckId_Lut
