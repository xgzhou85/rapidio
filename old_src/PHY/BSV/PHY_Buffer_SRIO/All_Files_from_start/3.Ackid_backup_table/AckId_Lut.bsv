package AckId_Lut;

import Vector::*;


interface Ifc_AckId_Lut;
method Action _ackid(Bit#(6) value);
method Action _prio_in(Bit#(2) value);
method Action _rd_ptr_in(Bit#(4) value);
method Action _identify(Bit#(2) value);

method Bit#(2) prio_out_();
method Bit#(4) rd_ptr_out_();
endinterface:Ifc_AckId_Lut

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkAckId_Lut(Ifc_AckId_Lut);



Vector#(64,Reg#(Bit#(12)))  rg_vect_lut <- replicateM(mkReg(0));

Wire#(Bit#(6)) wr_ack_id <- mkDWire(0);
Wire#(Bit#(4)) wr_rd_ptr_in <- mkDWire(0);
Wire#(Bit#(2)) wr_prio <- mkDWire(0);
Wire#(Bit#(2)) id <- mkDWire(0);
Wire#(Bit#(4)) wr_rd_ptr_out <- mkDWire(0);
Wire#(Bit#(2)) wr_prio_out <- mkDWire(0);




rule update(id == 2'b01);
let lv_id = wr_ack_id;
rg_vect_lut[lv_id] <= {wr_ack_id,wr_prio,wr_rd_ptr_in};
endrule

rule out_retrans(id == 2'b10);
let lv_id = wr_ack_id;
wr_rd_ptr_out <= rg_vect_lut[lv_id][3:0];
wr_prio_out <= rg_vect_lut[lv_id][5:4];
endrule

/*
rule update;
case(wr_ack_id)
6'b000000
6'b000001
6'b000010
6'b000011
6'b000100
6'b000101
6'b000110
6'b000111
6'b001000
6'b001001
6'b001010
6'b001011
6'b001100
6'b001101
6'b001110
6'b001111
6'b010000
6'b010001
6'b010010
6'b010011
6'b010100
6'b010101
6'b010110
6'b010111
6'b011000
6'b011001
6'b011010
6'b011011
6'b011100
6'b011101
6'b011110
6'b011111
6'b100000
6'b100001
6'b100010
6'b100011
6'b100100
6'b100101
6'b100110
6'b100111
6'b101000
6'b101001
6'b101010
6'b101011
6'b101100
6'b101101
6'b101110
6'b101111
6'b110000
6'b110001
6'b110010
6'b110011
6'b110100
6'b110101
6'b110110
6'b110111
6'b111000
6'b111001
6'b111010
6'b111011
6'b111100
6'b111101
6'b111110
6'b111111
*/
method Action _ackid(Bit#(6) value);
       wr_ack_id <= value;
endmethod

method Action _rd_ptr_in(Bit#(4) value);
       wr_rd_ptr_in <= value;
endmethod

method Action _prio_in(Bit#(2) value);
	wr_prio <= value;
endmethod
	
method Action _identify(Bit#(2) value);
       id <= value;
endmethod

method Bit#(4) rd_ptr_out_();
       return wr_rd_ptr_out;
endmethod

method Bit#(2) prio_out_();
	return wr_prio_out;
endmethod


endmodule:mkAckId_Lut
endpackage:AckId_Lut
