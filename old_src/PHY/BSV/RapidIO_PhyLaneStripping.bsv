/*
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    
-- RapidIO Physical Layer Lane-Stripping Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module is developed with the intention to perform, 
-- 1. Strip the incoming Data Packets and Control Symbol Packet and assign to four lane with each lane can carry 64 bits.
-- 2. Each lane (64 bits) are forwarded to 64b/67b encoding.
-- 3. The incoming Data Packet (128 bits) and Control signals are stored in a Packet ByPass buffer to perform lane Stripping and control symbol packet generation sequentially. 
-- 4. Internally, This module invokes Control Pkt Gen module to generate Control Symbol Packet (64 bits).
-- 5. Multiple clock domains are used in this design (2 different clocks def_CLK, laneCLK).
-- 6. Each lane has SyncFIFO where enqueue is performed with respect to source clock (User Interface Clock), and, dequeue and first performed with second clock.  
-- 7. The output of each lane transfers  data at the rate of second clock speed with 64 bits each.
-- 
-- 
-- To Do's
-- This module will be enhanced in future based on the specification.  
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package RapidIO_PhyLaneStripping;

import RapidIO_DTypes ::* ;
import FIFO ::* ;
import FIFOF ::* ;
import SpecialFIFOs ::* ;
import Clocks ::* ;
import DefaultValue ::* ;
import RapidIO_PhyControlPktGen ::* ;

interface Ifc_RapidIO_PhyLaneStripping;

// Input Methods
 // User Interface Signals
 method Action _link_tx_sof_n (Bool value);
 method Action _link_tx_eof_n (Bool value);
 method Action _link_tx_vld_n (Bool value);
 // method Bool link_tx_rdy_n_ (); 
 method Action _inputs_DataPkt(Bit#(128) value);
// method Action _inputs_TxRem (Bit#(4) value);
// method Action _inputs_CRF (Bool value);

 // Control Symbol signals 
// method Action _inputs_CtrlSymbolPkt(Bit#(64) value);

// Output Methods  
 method Bit#(64) outputs_lane1_();
 method Bit#(64) outputs_lane2_();
 method Bit#(64) outputs_lane3_();
 method Bit#(64) outputs_lane4_();

// interface Clock laneCLK;
endinterface : Ifc_RapidIO_PhyLaneStripping

(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_PhyLaneStripping (Clock laneCLK, Reset laneRST, Ifc_RapidIO_PhyLaneStripping ifc); // For multiple Clock domains, Follow this syntax 
// -- External Clock - laneCLK, External Reset - laneRST
// Inputs and Outputs as Wires
// -- Incoming Data Packet
Wire#(Bool) wr_UISOF <- mkDWire (False);
Wire#(Bool) wr_UIEOF <- mkDWire (False);
Wire#(Bool) wr_UIVLD <- mkDWire (False);
Wire#(Bit#(128)) wr_DataPkt <- mkDWire(0);
Wire#(Bit#(4)) wr_UITxRem <- mkDWire (0);
Wire#(Bool) wr_UICRF <- mkDWire (False);

// -- Incoming Control Symbol Packet
Wire#(Bit#(64))  wr_CtrlPkt <- mkDWire(0);

// -- Registers are used to delay the control signals for a cycle. 
Reg#(Bool) rg_UISOF <- mkReg (False);
Reg#(Bool) rg_UIEOF <- mkReg (False);
Reg#(Bool) rg_UIVLD <- mkReg (False);
Reg#(Transmit_Pkt) rg_Data_From_FIFO <- mkReg(defaultValue);
Wire#(Transmit_Pkt) wr_Data_From_FIFO <- mkDWire(defaultValue);
Reg#(Bit#(2)) rg_CountValue <- mkReg (0); // Used to to determine the lane 
Reg#(Bool) rg_TransmitFIFO_Empty <- mkReg (True); // Delay the status of the FIFO by a cycle. 

// Default Clock and Reset
Clock def_CLK <- exposeCurrentClock(); // Default System Clock
Reset def_RST <- exposeCurrentReset(); // Default System Reset

/*
// New clock for lane stripping 
Reg#(Bit#(1)) osc <- mkReg(0);
//MakeClockIfc#(Bit#(1)) secondCLK <- mkUngatedClock(1'b0);
Clock laneCLK = secondCLK.new_clk; // Second CLOCK (Lane Clock)
rule rl_oscilate;
    let new_osc = invert(osc);
    secondCLK.setClockValue(new_osc);
//    secondCLK.setGateCond(gateClk);
    osc <= new_osc;
endrule

// New Reset for lane stripping
//MakeResetIfc secondRST <- mkAsyncReset(0, def_RST, laneCLK);
//Reset laneRST = secondRST.new_rst; // Second Reset
Reset laneRST <- mkAsyncReset(0, def_RST, laneCLK); // Second Reset

rule rl_secondReset;
    secondRST.assertReset();
endrule
*/

// Outputs as Wires - 4 Lanes (Each 64 bits long)
Wire#(Bit#(64))  wr_Lane0 <- mkDWire(0, clocked_by laneCLK, reset_by laneRST); // Output Wire for Lane 0 Clocked by laneCLK and Reseted by laneRST
Wire#(Bit#(64))  wr_Lane1 <- mkDWire(0, clocked_by laneCLK, reset_by laneRST); // Output Wire for Lane 1 Clocked by laneCLK and Reseted by laneRST
Wire#(Bit#(64))  wr_Lane2 <- mkDWire(0, clocked_by laneCLK, reset_by laneRST); // Output Wire for Lane 2 Clocked by laneCLK and Reseted by laneRST
Wire#(Bit#(64))  wr_Lane3 <- mkDWire(0, clocked_by laneCLK, reset_by laneRST); // Output Wire for Lane 3 Clocked by laneCLK and Reseted by laneRST


/*
-- SyncFIFO is used to operate the FIFOs under different clocks. FIFO enqueue operates with respect to def_CLK.
-- FIFO dequeue and FIFO first are operated with respect to laneCLK. 
-- Since def_CLK is much faster than laneCLK, it requires huge buffer size for packet and control. 
-- The Data is sent through these FIFOs sequentially to maintain in order transmission.
-- Bluespec Module : mkSyncFIFOFromCC is used which operates with Default System Clock and laneCLK.
*/   
// SynchFIFO Instantiation - for storing the Control and Data Packets for each lane. 
SyncFIFOIfc#(Bit#(64)) fifo_lane0 <- mkSyncFIFOFromCC(32, /*def_CLK, def_RST,*/ laneCLK); // SyncFIFO - Lane 0
SyncFIFOIfc#(Bit#(64)) fifo_lane1 <- mkSyncFIFOFromCC(32, /*def_CLK, def_RST,*/ laneCLK); // SyncFIFO - Lane 1
SyncFIFOIfc#(Bit#(64)) fifo_lane2 <- mkSyncFIFOFromCC(32, /*def_CLK, def_RST,*/ laneCLK); // SyncFIFO - Lane 2
SyncFIFOIfc#(Bit#(64)) fifo_lane3 <- mkSyncFIFOFromCC(32, /*def_CLK, def_RST,*/ laneCLK); // SyncFIFO - Lane 3

// ByPass FIFO - Transmit Packet
FIFOF#(Transmit_Pkt) ff_TransmitPktFIFO <- mkSizedBypassFIFOF (8); // The Depth of the FIFO is 8 and it can store upto 8 packets. 
Ifc_RapidIO_PhyControlPktGen mod_ControlPktGen <- mkRapidIO_PhyControlPktGen (); // Control Packet Generator Module Instantiation

/*
-- Function Action is used to enqueue 64 bit data to SyncFIFO for lane stripping.
-- Inputs countValue indicates to which FIFO, the packet has to be enqueued and PktInfo contains 64 bit data. 
*/
function Action fn_SyncFIFO_Enqueue (Bit#(2) countValue, Bit#(64) pktInfo);
    action
        case (countValue) matches
            'd0 : fifo_lane0.enq (pktInfo);
            'd1 : fifo_lane1.enq (pktInfo);
            'd2 : fifo_lane2.enq (pktInfo);
            'd3 : fifo_lane3.enq (pktInfo);
        endcase
    endaction
endfunction
		
// -- Rules
/*
-- The User Transmit Link Interface signals are enqueued in a FIFO. 
-- FIFOF type which is belongs to SizedByPass FIFO package, has the FIFO depth of value 'd8 and is also used to perform both dequeue and enqueue in the same cycle. 
-- This FIFO is used to process the packets in a sequential order
-- In addition, ByPass FIFO does not consume additional cycle. 
*/   
rule rl_Transmit_Pkt_FIFO_Enqueue (wr_UIVLD == True);
    ff_TransmitPktFIFO.enq(Transmit_Pkt {sof: wr_UISOF, eof: wr_UIEOF, vld: wr_UIVLD, data: wr_DataPkt, txrem: wr_UITxRem, crf: wr_UICRF});
endrule

/*
-- After the Tranmit Link interface enqueued, then it is dequeued and sent to Contrl Symbol Packet Generation module to generate Control Symbol Packet
-- It also delays Transmit Link Interface for a cycle allowing Control Packet Generation to take place
*/   
rule rl_Mapping_TransmitFIFO_First_To_CtrlPktGeneration;
    if (ff_TransmitPktFIFO.notEmpty() == True) begin // Valid signal is sent to Control Symbol Packet Generation
        let lv_DataPkt_from_FIFO = ff_TransmitPktFIFO.first();
        wr_Data_From_FIFO <= lv_DataPkt_from_FIFO; // Delay the Transmit Link Interface Signals for a cycle and sent for Lane Stripping
        mod_ControlPktGen._inputs_link_tx_sof_n (!lv_DataPkt_from_FIFO.sof);
        mod_ControlPktGen._inputs_link_tx_eof_n (!lv_DataPkt_from_FIFO.eof);
        mod_ControlPktGen._inputs_link_tx_vld_n (!lv_DataPkt_from_FIFO.vld);
    end
    else if (ff_TransmitPktFIFO.notEmpty() == False) begin // In this case, default value is sent to Control Packet Generation module
        wr_Data_From_FIFO <= defaultValue; 
        mod_ControlPktGen._inputs_link_tx_sof_n (True);
        mod_ControlPktGen._inputs_link_tx_eof_n (True);
        mod_ControlPktGen._inputs_link_tx_vld_n (True);
    end
endrule

// Delaying the User Interface signals for a cycle wherein, during this period the Control Symbol packet is generated. 
rule rl_Delay_Data_From_FIFO;
    rg_Data_From_FIFO <= wr_Data_From_FIFO;
endrule

/*
-- Transmit Packet FIFO Dequeue is performed whenever the FIFO notEmpty signal is enabled.    
-- There is no point in dequeuing the empty FIFO. 
*/   
rule rl_Transmit_FIFO_Dequeue (ff_TransmitPktFIFO.notEmpty() == True);
    ff_TransmitPktFIFO.deq();
endrule

/*
-- Check the status of the Tranmit Link Interface FIFO
-- Similar to User Interface Signals, the status signal of the FIFO is also delayed by a cycle. 
*/   
rule rl_Transmit_FIFO_Status;
    if (ff_TransmitPktFIFO.notEmpty() == True) begin
        rg_TransmitFIFO_Empty <= False;
    end
    else 
        rg_TransmitFIFO_Empty <= True; 
endrule 

/*
-- Following rule performs Lane Stripping.
-- Since the design uses 4 lanes, a 2 bit counter is used to identify the particular lane. 
-- The 2 bit counter also used to perform serialization. 
-- Every cycle, counter add to itself by a value of 3 for Data packets and Control Packet that to be sent to the corresponding lane. 
-- Above defined function is called inside the following rule to assign Data packets and Control Packet to each lane based on the Counter Value. 
-- If it is single packet transaction, Control symbol packet (contains both SOF and EOF Info) are generated and sent first, followed by Data packets. 
-- During SOF, Control Packet (containing SOF info) is sent first and followed by Data Packets in order to maintain sequence.
-- During EOF, Control Packet (containing EOF info) is sent after the Data Packets in order to maintain sequence.
-- Every cycle, Data Packets (128 bits) and Control Symbol Packet (64 bits) are enqueued to each lanes using SyncFIFO. 
*/
rule rl_LaneStripping (rg_TransmitFIFO_Empty == False);
    Bit#(64) lv_ControlPktValid = 0; // It holds the valid control symbol packet
    let lv_ControlPktGen = mod_ControlPktGen.outputs_ControlSymbolPkt_();
    if (lv_ControlPktGen matches tagged Valid .controlData) 
        lv_ControlPktValid = controlData;
    else 
        lv_ControlPktValid = 0;

    if (rg_Data_From_FIFO.vld == True) begin
        if ((rg_Data_From_FIFO.sof == True) && (rg_Data_From_FIFO.eof == True)) begin
            rg_CountValue <= rg_CountValue + 'd3;
            let lv_FIFO_Value0 = rg_CountValue;
            let lv_FIFO_Value1 = rg_CountValue + 'd1;
            let lv_FIFO_Value2 = rg_CountValue + 'd2; 

            fn_SyncFIFO_Enqueue (lv_FIFO_Value0, lv_ControlPktValid);
            fn_SyncFIFO_Enqueue (lv_FIFO_Value1, rg_Data_From_FIFO.data[127:64]);
            fn_SyncFIFO_Enqueue (lv_FIFO_Value2, rg_Data_From_FIFO.data[63:0]);

        end
        else if ((rg_Data_From_FIFO.sof == True) && (rg_Data_From_FIFO.eof == False)) begin
            rg_CountValue <= rg_CountValue + 'd3;
            let lv_FIFO_Value0 = rg_CountValue;
            let lv_FIFO_Value1 = rg_CountValue + 'd1;
            let lv_FIFO_Value2 = rg_CountValue + 'd2; 

            fn_SyncFIFO_Enqueue (lv_FIFO_Value0, lv_ControlPktValid);
            fn_SyncFIFO_Enqueue (lv_FIFO_Value1, rg_Data_From_FIFO.data[127:64]);
            fn_SyncFIFO_Enqueue (lv_FIFO_Value2, rg_Data_From_FIFO.data[63:0]);
        end
        else if ((rg_Data_From_FIFO.sof == False) && (rg_Data_From_FIFO.eof == True)) begin
            rg_CountValue <= rg_CountValue + 'd3;
            let lv_FIFO_Value0 = rg_CountValue;
            let lv_FIFO_Value1 = rg_CountValue + 'd1;
            let lv_FIFO_Value2 = rg_CountValue + 'd2; 

            fn_SyncFIFO_Enqueue (lv_FIFO_Value0, rg_Data_From_FIFO.data[127:64]);
            fn_SyncFIFO_Enqueue (lv_FIFO_Value1, rg_Data_From_FIFO.data[63:0]);
            fn_SyncFIFO_Enqueue (lv_FIFO_Value2, lv_ControlPktValid);
        end
        else begin
            rg_CountValue <= rg_CountValue + 'd3;
            let lv_FIFO_Value0 = rg_CountValue;
            let lv_FIFO_Value1 = rg_CountValue + 'd1;
            let lv_FIFO_Value2 = rg_CountValue + 'd2; 

            fn_SyncFIFO_Enqueue (lv_FIFO_Value0, rg_Data_From_FIFO.data[127:64]);
            fn_SyncFIFO_Enqueue (lv_FIFO_Value1, rg_Data_From_FIFO.data[63:0]);
            fn_SyncFIFO_Enqueue (lv_FIFO_Value2, lv_ControlPktValid);
        end
    end
endrule

/*
-- Following rules, check the status of the notEmpty signals of 4 SyncFIFOs, and, performs the read and dequeue operation of 4 SyncFIFOs  
*/   
rule rl_ReadData_From_SyncFIFO_0; // SyncFIFO_0
    if (fifo_lane0.notEmpty == True) begin
        wr_Lane0 <= fifo_lane0.first;
        fifo_lane0.deq();
    end
    else if (fifo_lane0.notEmpty == False)
        wr_Lane0 <= 0;
endrule


rule rl_ReadData_From_SyncFIFO_1; // SyncFIFO_1
    if (fifo_lane1.notEmpty == True) begin
        wr_Lane1 <= fifo_lane1.first;
        fifo_lane1.deq();
    end
    else if (fifo_lane1.notEmpty == False)
        wr_Lane1 <= 0;
endrule


rule rl_ReadData_From_SyncFIFO_2; // SyncFIFO_2
    if (fifo_lane2.notEmpty == True) begin
        wr_Lane2 <= fifo_lane2.first;
        fifo_lane2.deq();
    end
    else if (fifo_lane2.notEmpty == False)
        wr_Lane2 <= 0;
endrule        


rule rl_ReadData_From_SyncFIFO_3; // SyncFIFO_3
    if (fifo_lane3.notEmpty == True) begin
        wr_Lane3 <= fifo_lane3.first;
        fifo_lane3.deq();
    end
    else if (fifo_lane3.notEmpty == False)
        wr_Lane3 <= 0; 
endrule


// -- Delaying the control signals by a cycle for lane stripping
rule rl_Delay_CtrlSignals;
    rg_UISOF <= wr_UISOF;
    rg_UIEOF <= wr_UIEOF;
    rg_UIVLD <= wr_UIVLD;
endrule

rule rl_Display_FIFO_Lane_Status;
    $display ("\nThe Status of FIFO Lane0 Not Empty == %b", fifo_lane0.notEmpty());
    $display ("\nThe Status of FIFO Lane1 Not Empty == %b", fifo_lane1.notEmpty());
    $display ("\nThe Status of FIFO Lane2 Not Empty == %b", fifo_lane2.notEmpty());
    $display ("\nThe Status of FIFO Lane3 Not Empty == %b", fifo_lane3.notEmpty());
endrule

// -- Input and Output Methods Definition 
 // Input Methods
 method Action _link_tx_sof_n (Bool value);
    wr_UISOF <= !value;
 endmethod 
 method Action _link_tx_eof_n (Bool value);
    wr_UIEOF <= !value;
 endmethod
 method Action _link_tx_vld_n (Bool value);
    wr_UIVLD <= !value; 
 endmethod
 method Action _inputs_DataPkt(Bit#(128) value);
    wr_DataPkt <= value;
 endmethod
 /*
 method Action _inputs_TxRem (Bit#(4) value);
    wr_UITxRem <= value; 
 endmethod
 method Action _inputs_CRF (Bool value);
    wr_UICRF <= value; 
 endmethod


 method Action _inputs_CtrlSymbolPkt(Bit#(64) value);
    wr_CtrlPkt <= value;
 endmethod
 */

 // Output Methods
 method Bit#(64) outputs_lane1_();
    return wr_Lane0;
 endmethod
 method Bit#(64) outputs_lane2_();
    return wr_Lane1;
 endmethod
 method Bit#(64) outputs_lane3_();
    return wr_Lane2;
 endmethod					
 method Bit#(64) outputs_lane4_();
    return wr_Lane3;
 endmethod

endmodule : mkRapidIO_PhyLaneStripping
endpackage : RapidIO_PhyLaneStripping
						
