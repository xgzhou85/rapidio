/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Author(s):
-- Anshu Kumar (akgeni@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/
package RapidIO_Cache_Coherence_L2ControlInterface;


import RapidIO_Cache_Coherence_Encode_packet::*;
import RapidIO_Cache_Coherence_Types ::*;
import RapidIO_Cache_Coherence_L2Contrl ::*;
import RapidIO_Cache_Coherence_GSM ::*;
import RapidIO_DTypes::*;


interface Ifc_L2_CacheContrl;




//method Action _inputs_packet_L2Contrl(Packet pkt);
method Action _inputs_Processor_Agent_Is_Ready(Bool value);
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Local(Packet pkt);
method Action _inputs_From_Processor_Agent_To_L2contrl_Resp(Packet pkt);
method Action _inputs_packet_To_L2Contrl_From_processor_Agent_Resp(Packet pkt);

method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Remote(Packet pkt);
method Action _load_From_Memory_And_Change_State(Physical_Address_Type physical_Address,State s);

method Packet _outputs_Packet_L2Contrl_To_Processor_Agent_Resp();

//method Action _inputs_next_request();
//method Action _inputs_From_Processor_Agent_To_L2contrl_Resp_Local(Packet pkt);
//method Action _inputs_From_Processor_Agent_To_L2contrl_Resp(Packet pkt); 

method Packet _get_data_L2Contrl(Packet pkt);
method Packet _outputs_Packet_L2Contrl();
method State  _gets_status_block(Physical_Address_Type physical_Address);
//method Packet _outputs_Packet_L2Contrl_To_Processor_Agent_Resp();   // At Remote
method Packet _outputs_data_mkTb(); 
method Bool _outputs_AlterCalled();
//method Packet _outputs_Request_To_Processor_Agent();

 // Local Response


method ActionValue#(Packet) _inputs_packet_L2Contrl_From_RapidIO_Req(Packet pkt); // At Remote.
method ActionValue#(Packet) _outputs_Request_To_Processor_Agent();
method Action _inputs_From_Processor_Agent_To_L2contrl_alterState(Packet pkt);


endinterface


endpackage
