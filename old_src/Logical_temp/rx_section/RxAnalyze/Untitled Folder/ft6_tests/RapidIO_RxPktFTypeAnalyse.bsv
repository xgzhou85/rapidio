package RapidIO_RxPktFTypeAnalyse;

`include "RapidIO.defines"

import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import DefaultValue ::*;
import RapidIO_RxFTypeFunctionsDev8 ::*;
import RapidIO_RxFTypeFunctionsDev16 ::*;
import RapidIO_PktTransportParse ::*;
import RapidIO_TgtDecoder_ByteCnt_ByteEn ::*;


interface Ifc_RapidIO_RxPktFTypeAnalyse;

 method Action _inputs_ReceivedPkts (ReceivedPktsInfo value); // Input Packets Received from the Incoming Separation Module 
 method Action _inputs_RxFtype6StreamClass (Maybe#(FType6_StreamWrClass) value); // Ftype6 Logical Packets
 method Action _inputs_RxFtype6StreamData (Maybe#(Ftype6StreamData) value); // Ftype6 Incoming Stream Data 

 method Action _inputs_TTReceived (TT value); // TT received from the Incoming packet 
 method Action _inputs_RxDestId (DestId value); // Decoded destination ID
 method Action _inputs_RxSourceId (SourceId value); // Decoded Source ID
 method Action _inputs_RxPrioField (Prio value); // Decoded Priority Field
 method Action _inputs_MaxPktCount (Bit#(4) value); // Carries the Maximum Packet Received in the Current Transaction

 method Action _inputs_TxReady_From_IResp (Bool value); // 
 method Action _inputs_TxReady_From_TReq (Bool value); // 
 method Action _inputs_TxReady_From_MReq (Bool value); // 

 // Output Methods as Ports
 method Maybe#(TargetReqIfcPkt) outputs_TargetReqIfcPkt_ (); // Output Target Request Signals 

 method Bool outputs_TxReadyOut_From_Analyze_ ();

endinterface : Ifc_RapidIO_RxPktFTypeAnalyse


(* synthesize *)

module mkRapidIO_RxPktFTypeAnalyse (Ifc_RapidIO_RxPktFTypeAnalyse);

Wire#(ReceivedPktsInfo) wr_RxPktInfo <- mkDWire (defaultValue);
Wire#(Maybe#(FType6_StreamWrClass)) wr_RxFtype6StreamPkt <- mkDWire (tagged Invalid);
Reg#(Maybe#(FType6_StreamWrClass)) rg_RxFtype6StreamPkt <- mkReg (tagged Invalid);

  // Ftype Data 
Wire#(Maybe#(Ftype6StreamData)) wr_RxFtype6StreamData <- mkDWire (defaultValue);

Reg#(Maybe#(Ftype6StreamData)) rg_RxFtype6StreamData <- mkReg (defaultValue);

Wire#(DestId) wr_RxDestId <- mkDWire (0);
Wire#(SourceId) wr_RxSourceId <- mkDWire (0);
Wire#(Prio) wr_RxPrioField <- mkDWire (0);
Wire#(Bit#(4)) wr_MaxPktCount <- mkDWire (0);
Wire#(TT) wr_RxTT <- mkDWire (0); 
Reg#(TT) rg_RxTT <- mkReg (0); 

/*
// Initiator Response Signals
Wire#(InitRespIfcCntrl) wr_InitRespIfcCntrl <- mkDWire (defaultValue); // Initiator Response Control Signal
Wire#(InitRespIfcData) wr_InitRespIfcData <- mkDWire (defaultValue); // Initiator Response Data Signal
Wire#(InitRespIfcMsg) wr_InitRespIfcMsg <- mkDWire (defaultValue); // Initiator Response Message Signal 
Wire#(Maybe#(InitiatorRespIfcPkt)) wr_InitRespIfcPkt <- mkDWire (defaultValue); // Initiator Response Signals Concatenated to a Single Packet
*/
// Target Request Signals
Wire#(TargetReqIfcCntrl) wr_TgtReqIfcCntrl <- mkDWire (defaultValue); // Target Request Control Signal
Wire#(TargetReqIfcData) wr_TgtReqIfcData <- mkDWire (defaultValue); // Target Request Data Signal
Wire#(TargetReqIfcMsg) wr_TgtReqIfcMsg <- mkDWire (defaultValue); // Target Request Message Signal
Wire#(Maybe#(TargetReqIfcPkt)) wr_TgtReqIfcPkt <- mkDWire (defaultValue); // Target Request Signals concatenated to a Single Packet
/*
// Maintenance Request Signals
Wire#(MaintenanceReqIfcCntrl) wr_MReqIfcCntrl <- mkDWire (defaultValue); // Maintenance Request Control Signal
Wire#(MaintenanceReqIfcData) wr_MreqIfcData <- mkDWire (defaultValue); // Maintenance Request Data Signal
Wire#(Maybe#(MaintenanceReqIfcPkt)) wr_MreqIfcPkt <- mkDWire (tagged Invalid); // Maintenance Signals concatenated to a Single Packet 
*/
// Internal Wires and Registers
Reg#(ByteCount) rg_RxByteCount <- mkReg (0);  // Byte Count Value calculation for Ftype 2 and Ftype 5 
Reg#(ByteEn) rg_RxByteEn <- mkReg (0); // Byte Count Value calculation for Ftype 2 and Ftype 5 
Reg#(Bit#(4)) rg_PktCount <- mkReg (0); // Delayed PktCount
Reg#(Bool) rg_ByteCountValid <- mkReg (False);
Reg#(Bit#(56)) rg_TmpStreamDataDev16 <- mkReg (0);
Reg#(Bit#(8)) rg_Ftype6TempDataDev8 <- mkReg (0);
Reg#(Data) rg_Ftype6StreamData <- mkReg (0);
Reg#(Bool) rg_LastDataDev16 <- mkReg (False);


// Input Ready Signals from Initiator Response, Target Request and Maintenance Request 
Wire#(Bool) wr_TxRdy_IRespIn <- mkDWire (False);
Wire#(Bool) wr_TxRdy_TReqIn <- mkDWire (False);
Wire#(Bool) wr_TxRdy_MReqIn <- mkDWire (False);

Reg#(Ftype6StreamData)  rg_StreamData <- mkReg(defaultValue);

Reg#(TT) rg_RxTT_delay <- mkReg (0); 

// Module Instantiation 
// Ifc_RapidIO_TgtDecoder_ByteCnt_ByteEn mod_SizeToByteCountByteEnConverter <- mkRapidIO_TgtDecoder_ByteCnt_ByteEn ();

// -- Rules -- 
rule rl_DelayedPktCount; // Rule to delay the Byte Count 
    rg_PktCount <= wr_RxPktInfo.pktcount;
endrule


rule rxtt;
rg_RxTT_delay <= rg_RxTT;
 wr_RxTT <= rg_RxTT_delay;
	//wr_RxFtype6StreamData <= rg_RxFtype6StreamData;
endrule

rule rl_TargetRequestIfcGen;
$display("11111111111111111111111111111111");
  
    if (wr_RxFtype6StreamPkt matches tagged Valid .ftype6) begin // Ftype6 Logical Packet Decoding
$display("22222222222222222222222222222222");
     	Bool lv_Valid = False; 
     	Ftype6StreamData lv_StreamData = defaultValue; // Ftype6 Stream Data

        if (wr_RxFtype6StreamData matches tagged Valid .streamdata) begin // Validates the VALID and Data Signals 
$display("333333333333333333333333333333333");
            lv_StreamData = streamdata;
            lv_Valid = True;
       	end
        else begin 

            lv_StreamData = defaultValue;
            lv_Valid = False; 
        end
    	
    	 if (wr_RxTT == 2'b01) begin // Dev16 Support 
$display("4444444444444444444444444444444444");
        	Bit#(29) lv_AddrDev16 = 0;
        	Bit#(2) lv_XamsbsDev16 = 0;
        	Data lv_Ftype6Data = 0;
        	Bit#(8) lv_TempStreamDataDev16 = 0; 
        	Bool lv_Last = False; 
  		Bool lv_EOF = False; 
        	Bit#(4) lv_PktCount = 0;

        	lv_PktCount = wr_RxPktInfo.pktcount;


        	if (lv_StreamData.ftype6LastData == True) begin // Validates EOF 
            		lv_Last = True; 
            		lv_EOF = True; 
        	end
        	else begin 
            		lv_Last = False; 
            		lv_EOF = False;
        	end
       
	        TargetReqIfcCntrl lv_treqcntrl = TargetReqIfcCntrl {treq_sof: (lv_PktCount >= 'd0) ? True : False, 
							    treq_eof: lv_EOF, 
							    treq_vld: lv_Valid};
		TargetReqIfcData lv_treqdata = TargetReqIfcData {treq_tt: wr_RxTT, 
	treq_data:((lv_PktCount >= 'd0) ? lv_StreamData.ftype6Data :0), 
						treq_crf: False, treq_prio: wr_RxPrioField, 
						treq_ftype: ((lv_PktCount >= 'd0) ? ftype6.ftype : 0), 
						treq_destid: ((lv_PktCount >= 'd0) ? wr_RxDestId : 0), 
						treq_sourceid: ((lv_PktCount >= 'd0) ? wr_RxSourceId : 0), treq_tid: defaultValue, 
						treq_ttype: 0, treq_addr: ((lv_PktCount >= 'd0) ? {ftype6.xamsbs, ftype6.addr, 3'b000} : 0), 
						treq_byte_count: 0/*((lv_PktCount == 'd2) ? mod_SizeToByteCountByteEnConverter.outputs_ByteCount_ () : 0)*/, 
						treq_byte_en: 0/*mod_SizeToByteCountByteEnConverter.outputs_ByteEn_ ()*/};
		TargetReqIfcMsg lv_treqmsg = TargetReqIfcMsg {treq_db_info: defaultValue, treq_msg_len: defaultValue, treq_msg_seg: defaultValue,
					      treq_mbox: defaultValue, treq_letter: defaultValue};

$display("################ ftype 6 data ####################### == %h ",lv_StreamData.ftype6Data);
	    	wr_TgtReqIfcPkt <= tagged Valid TargetReqIfcPkt {treqcntrl : lv_treqcntrl,
							 treqdata : lv_treqdata,
							 treqmsg: lv_treqmsg};
     	end 
    
   end
endrule

 method Action _inputs_ReceivedPkts (ReceivedPktsInfo value);
	wr_RxPktInfo <= value;
 endmethod

 method Action _inputs_RxFtype6StreamClass (Maybe#(FType6_StreamWrClass) value);
	wr_RxFtype6StreamPkt <= value;
	rg_RxFtype6StreamPkt <= value;
 endmethod

 method Action _inputs_RxFtype6StreamData (Maybe#(Ftype6StreamData) value);
	wr_RxFtype6StreamData <= value;
	rg_RxFtype6StreamData <= value;
 endmethod

 method Action _inputs_TTReceived (TT value);
	//wr_RxTT <= value; 
	rg_RxTT <= value; 
 endmethod
 method Action _inputs_RxDestId (DestId value);
	wr_RxDestId <= value;
 endmethod
 method Action _inputs_RxSourceId (SourceId value);
	wr_RxSourceId <= value;
 endmethod
 method Action _inputs_RxPrioField (Prio value);
	wr_RxPrioField <= value;
 endmethod
 method Action _inputs_MaxPktCount (Bit#(4) value);
	wr_MaxPktCount <= value; 
 endmethod

 method Action _inputs_TxReady_From_IResp (Bool value); // 
	wr_TxRdy_IRespIn <= value;
 endmethod
 method Action _inputs_TxReady_From_TReq (Bool value); // 
 	wr_TxRdy_TReqIn <= value; 
 endmethod
 method Action _inputs_TxReady_From_MReq (Bool value);
	wr_TxRdy_MReqIn <= value; 
 endmethod

 method Maybe#(TargetReqIfcPkt) outputs_TargetReqIfcPkt_ ();
	return wr_TgtReqIfcPkt;
 endmethod

 method Bool outputs_TxReadyOut_From_Analyze_ ();
	return (wr_TxRdy_IRespIn && wr_TxRdy_TReqIn && wr_TxRdy_MReqIn);
 endmethod 
endmodule : mkRapidIO_RxPktFTypeAnalyse


endpackage : RapidIO_RxPktFTypeAnalyse

