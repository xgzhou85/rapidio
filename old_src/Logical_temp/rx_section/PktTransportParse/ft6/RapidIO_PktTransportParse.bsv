/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Incoming Packet Parsing Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module developed, 
-- 1. After the incoming packets are separated as Header and Data Packets, it is forwarded 
--    to this module for further processing.
-- 2. The Header and Data packets are analyzed and logical layer ftype packets are generated.
-- 3. This module invokes RxFtypeFunctions package to depacketize the incoming packets.
-- 4. Supports both Dev8 and Dev16 fields. 
--
-- To Do's
-- Yet to Complete 
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
--	-- Packet Description (Dev8 Support) -- 
--
-- The incoming packets for different format type (FType) are shown below. 

1. Request Class (Atomic or NRead Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], rdsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0] }

2. Write Request Class (Atomic or NWRITE Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], wrsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

3. Stream Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], Address[28:0], Resv, Xamsbs[1:0], Data1[63:0], 8'h00 }
	Data_2 -> { Data2, 64'h0 } // This packet will extend, if there are more data but the number of bytes in the packet should not exceed 80 bytes (10 Packets)

4. Maintenance Read Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], rdsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], wrsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv, Data[63:8] }
	Data_1 -> { Data[7:0] }

6. Maintenance Read Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[15:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

7. Maintenance Write Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[15:0] }

8. Response With Data 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Data[63:0] }

9. Response Without Data
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0] }

--------------------------------------------------------------------------------------------------------------------------------------------------------
--      -- Packet Description (Dev16 Support) -- 
--
-- The incoming packets for different format type (FType) are shown below. 

1. Request Class (Atomic or NRead Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], rdsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0] }

2. Write Request Class (Atomic or NWRITE Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], wrsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0], Data[63:24] }
	Data_1 -> { Data[23:0] }

3. Stream Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], Address[28:0], Resv, Xamsbs[1:0], Data1[63:8] }
	Data_1 -> { Data1[7:0], Data2 } 
	Data_2 -> { 8'h00, Data3 } // This packet will extend, if there are more data but the number of bytes in the packet should not exceed 80 bytes (10 Packets)

4. Maintenance Read Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], rdsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], wrsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv, Data[63:24] }
	Data_1 -> { Data[23:0] }

6. Maintenance Read Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[23:0], Data[63:24] }
	Data_1 -> { Data[23:0] }

7. Maintenance Write Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[23:0] }

8. Response With Data 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Data[63::0] }

9. Response Without Data
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0] }

--------------------------------------------------------------------------------------------------------------------------------------------------------
--
--        
*/

package RapidIO_PktTransportParse;

import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import DefaultValue ::*;
import RapidIO_RxFTypeFunctionsDev8 ::*;
import RapidIO_RxFTypeFunctionsDev16 ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_TgtDecoder_ByteCnt_ByteEn ::*;


`include "RapidIO.defines"

/*
-- Incoming Signals are same as the Transmitter signals.
-- SOF, EOF, Valid, Ready, Data (Packet), RxRem, CRF. 
-- Input Signals are given through Method Action 
-- Output Signals are obtained through Method return. 
--
-- The Incoming packet is decoded and obtained at the output 
-- in Ftype format packets. 
-- Data also decoded and resulted separately. 
*/


interface Ifc_RapidIO_PktTransportParse;
 // Input Ports as Methods
 method Action _PktParseRx_SOF_n (Bool value); 
 method Action _PktParseRx_EOF_n (Bool value);
 method Action _PktParseRx_VLD_n (Bool value);
 method Bool link_rx_rdy_n_ ();

 method Action _PktParseRx_data (DataPkt value);
 method Action _PktParseRx_rem (Bit#(4) value);
 method Action _PktParseRx_crf (Bool value);

 method Action _inputs_TxReadyIn_From_Analyze (Bool value);

 // Output Ports as Methods
 
 method Maybe#(FType6_StreamWrClass) outputs_RxFtype6StreamClass_ ();
 method Maybe#(Ftype6StreamData) outputs_RxFtype6StreamData_ ();
 method ReceivedPktsInfo outputs_ReceivedPkts_ ();

 /*
 -- The Transport Layer fields are decoded and removed from the packet and sent to Logical layer 
 -- outputs directly. 
 */
 method TT outputs_TTReceived_ ();
 method DestId outputs_RxDestId_ ();
 method SourceId outputs_RxSourceId_ ();
 method Prio outputs_RxPrioField_ ();
 method Bit#(4) outputs_MaxPktCount_ ();

endinterface : Ifc_RapidIO_PktTransportParse

/*
-- This type is defined to concatenate the Header packet, Data packet and Packet count for other modules for computation  
*/
typedef struct { DataPkt headerpkt;
		 DataPkt datapkt;
		 Bit#(4) pktcount;
		 Bool lastpkt;
} ReceivedPktsInfo deriving (Bits, Eq);

instance DefaultValue#(ReceivedPktsInfo); // Default Value assigned for the Received PktsInfo 
   defaultValue = ReceivedPktsInfo {headerpkt: 0, datapkt: 0, pktcount: 0, lastpkt: False };
endinstance


(* synthesize *)
//(*descending_urgency = "rl_FromIncomingPktSeparation,rl_Ftype6StreamWriteFormat" *)

module mkRapidIO_PktTransportParse (Ifc_RapidIO_PktTransportParse);

 // Input Methods as Wires 
Wire#(Bool) wr_PktParseRx_SOF <- mkDWire (False);
Wire#(Bool) wr_PktParseRx_EOF <- mkDWire (False);
Wire#(Bool) wr_PktParseRx_VLD <- mkDWire (False);

Wire#(DataPkt) wr_PktParseRx_data <- mkDWire (0);
Wire#(Bit#(4)) wr_PktParseRx_rem <- mkDWire (0);
Wire#(Bool) wr_PktParseRx_crf <- mkDWire (False);

///////   ##########################################################################
Wire#(DataPkt) wr_HeaderPkt <- mkDWire (0);

Reg#(DataPkt) rg_HeaderPkt <- mkReg (0); // Delay HeaderPkt for 1 clock cycle
Wire#(DataPkt) wr_DataPkt <- mkDWire (0);
Reg#(DataPkt) rg_DataPkt <- mkReg (0); // Delay DataPkt for 1 Clock Cycle
Wire#(Bit#(4)) wr_PktCount <- mkDWire (0);
Reg#(Bit#(4)) rg_PktCount <- mkReg (0);// Delay Pkt Count for 1 Clock Cycle
Reg#(Bool) rg_LastPkt <- mkReg (False); // Delayed the Last Packet valid bit 

Reg#(DataPkt) rg_datapacket <- mkReg (0); // Delay DataPkt for 1 Clock Cycle
Reg#(DataPkt) rg_datapacket_delay <- mkReg (0);
Reg#(DataPkt) rg_datapacket_delay_2 <- mkReg (0);

//////////##############################################

Reg#(TT) rg_TTReceived <- mkReg (0); 
Reg#(Prio) rg_PrioReceived <- mkReg (0);
Reg#(DestId) rg_DestIDReceived <- mkReg (0);
Reg#(SourceId) rg_SrcIDReceived <- mkReg (0);
Reg#(Bit#(8)) rg_HopCountReceived <- mkReg (0);
Reg#(Maybe#(Bit#(4))) rg_RxRem <- mkReg (tagged Invalid);

// Ftype Resisters to hold the logical ftype packets 
Reg#(Maybe#(FType6_StreamWrClass)) rg_Ftype6_StreamWrClass <- mkReg (tagged Invalid);


Reg#(DataPkt) rg_HeaderPktFtype6 <- mkReg (0);
Reg#(DataPkt) rg_HeaderPktFtype6_delay <- mkReg (0);
Reg#(DataPkt) rg_HeaderPktFtype6_delay_2 <- mkReg (0);

Wire#(DataPkt) wr_HeaderPktFtype6 <- mkDWire (0);

Wire#(Bool) wr_Ftype6LastPkt <- mkDWire (False);

Wire#(Bool) wr_TxReady_In <- mkDWire (False);

Reg#(Maybe#(Ftype6StreamData)) rg_StreamData <- mkReg (tagged Invalid);


Ifc_RapidIO_InComingPkt_Separation pkt_Separation <- mkRapidIO_InComingPkt_Separation ();





rule rl_FromIncomingPktSeparation;
    	wr_HeaderPkt <= pkt_Separation.outputs_HeaderPkt_ ();
    	rg_HeaderPkt <= pkt_Separation.outputs_HeaderPkt_ (); // Delay the Header Packet
    	wr_DataPkt <= pkt_Separation.outputs_DataPkt_ ();
    	rg_DataPkt <= pkt_Separation.outputs_DataPkt_ (); // Delay the Data Packet 
    	wr_PktCount <= pkt_Separation.outputs_PktCount_ ();
    	rg_PktCount <= pkt_Separation.outputs_PktCount_ (); // Delay the Packet Count 
    	rg_LastPkt <= pkt_Separation.outputs_LastPkt_ (); // Delayed the Last Packet valid bit 

        wr_Ftype6LastPkt <= pkt_Separation.outputs_LastPkt_ ();

endrule


rule header_data_packet;
        rg_HeaderPktFtype6 <= wr_HeaderPkt;
	    rg_HeaderPktFtype6_delay <= rg_HeaderPktFtype6;
		rg_HeaderPktFtype6_delay_2 <= rg_HeaderPktFtype6_delay;


		rg_datapacket <= wr_DataPkt;
		rg_datapacket_delay <= rg_datapacket;
		rg_datapacket_delay_2 <= rg_datapacket_delay;
endrule

rule display;
	$display("\nwire header == %h ",wr_HeaderPkt);
	$display("wire data == %h ",wr_DataPkt);
	$display("register data == %h ",rg_datapacket);
	$display("register data delay == %h",rg_datapacket_delay);
endrule

rule rl_HeaderDecode_IncomingPkt;
    if (pkt_Separation.outputs_PktCount_ () == 0) begin
	rg_DestIDReceived <= 0;
	rg_SrcIDReceived <= 0;
	rg_PrioReceived <= 0;
	rg_TTReceived <= 0; 
    end
    else begin
      	rg_TTReceived <= wr_HeaderPkt[117:116]; 
      	rg_PrioReceived <= wr_HeaderPkt[119:118];
      	if (wr_HeaderPkt[117:116] == 'b00) begin 
		rg_DestIDReceived <= {wr_HeaderPkt[111:104], 24'h0};
		rg_SrcIDReceived <= {wr_HeaderPkt[103:96], 24'h0};
      	end
      	else if (wr_HeaderPkt[117:116] == 'b01) begin
		rg_DestIDReceived <= {wr_HeaderPkt[111:96], 16'd0};
		rg_SrcIDReceived <= {wr_HeaderPkt[95:80], 16'd0};
      	end 
      	else begin 
		rg_DestIDReceived <= {wr_HeaderPkt[111:104], 24'd0};
		rg_SrcIDReceived <= {wr_HeaderPkt[103:96], 24'd0};
      	end
    end 
endrule

/*
-- Rule to decode the hop count value for the maintenance packet
*/
rule rl_HopCountDecode;
    if ((wr_HeaderPkt[115:112] == `RIO_FTYPE8_MAINTAIN) && (wr_PktCount != 0))
	rg_HopCountReceived <= wr_HeaderPkt[79:72];
    else 
	rg_HopCountReceived <= 0;	
endrule

/*
-- Rule is to determine the values of the RxRem.
-- RxRem is valid only when the eof is enabled  
-- Physical layer (2 bytes ) removed and rg_RxRem contains the value of the RxRem for the logical layer
*/
rule rl_RxRemValid;
    if (wr_PktParseRx_EOF == True)
	rg_RxRem <= tagged Valid (wr_PktParseRx_rem - 'd2);
    else 
	rg_RxRem <= tagged Invalid;
endrule

/*
rule rl_Ftype6StreamWriteFormat(wr_HeaderPkt[115:112] == `RIO_FTYPE6_STREAM_WR);
$display("1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
$display("Packet_count == %h",rg_PktCount);
	if (wr_HeaderPkt[115:112] == `RIO_FTYPE6_STREAM_WR) begin
		 if (wr_PktCount == 4'd2) begin
$display("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			rg_Ftype6_StreamWrClass <= (wr_HeaderPkt[117:116] == 2'b01) ? tagged Valid fn_Dev16Ftype6StreamPktHeader (wr_PktCount, wr_HeaderPkt) : tagged Valid fn_Dev8Ftype6StreamPktHeader (wr_PktCount, wr_HeaderPkt) ; 
end
		else begin
			rg_Ftype6_StreamWrClass <= tagged Invalid ;
		end
rg_StreamData <= tagged Invalid;
end
endrule

*/

Reg#(Bit#(128)) rg_TmpStreamDataDev8 <- mkReg (0);
Reg#(Bit#(128)) rg_TmpStreamDataDev16 <- mkReg (0); 
Reg#(Bit#(128)) rg_TmpStreamDataDev16_2 <- mkReg (0); 
Wire#(Bit#(128)) wr_TmpStreamDataDev16_2 <- mkReg (0); 
Reg#(Bit#(29)) rg_Ftype6AddrDev16 <- mkReg (0);
Reg#(Bit#(2)) rg_Ftype6XAMSBSDev16 <- mkReg (0);
Reg#(Bool) rg_Ftype6LastData <- mkReg (False);
Reg#(Bit#(4)) rg_Ftype6PktCount <- mkReg (0);

Reg#(Bool) rg_Ftype6LastPkt <- mkReg (False);


rule rl_Ftype6StreamWriteFormat; // Format Type 6

	
  	Bool lv_LastPktDev8 = pkt_Separation.outputs_LastPkt_ ();
        rg_Ftype6LastPkt <= pkt_Separation.outputs_LastPkt_ ();

  	Bit#(29) lv_AddrDev16 = 0;
  	Bit#(2) lv_XamsbsDev16 = 0; 
  	Ftype6StreamData lv_StreamData = defaultValue;
  	Bool lv_Ftype6LastData = False; 
  	Data lv_Ftype6Data = 0;
    	Bit#(128) lv_TempStreamDataDev8 = 0;
    	Bit#(128) lv_TempStreamDataDev16 = 0;

        rg_Ftype6PktCount <= pkt_Separation.outputs_PktCount_ ();


    
    	 if ((rg_HeaderPktFtype6[115:112] == `RIO_FTYPE6_STREAM_WR) && (rg_HeaderPktFtype6[117:116] == 2'b01)) begin // Dev16 Support 
        		if (wr_PktCount == 4'd0 || wr_PktCount == 4'd1) begin
			    rg_TmpStreamDataDev16 <= 0;
end
      			else if (wr_PktCount >= 4'd2 && wr_PktCount < 4'd15 ) begin
        		    lv_TempStreamDataDev16 = {rg_HeaderPktFtype6_delay[31:0], rg_datapacket[127:32]}; 
			    rg_TmpStreamDataDev16 <= {rg_datapacket[31:0],wr_DataPkt[127:32]}; 
      			end
                     
/*
			 else if(wr_PktCount == 4'd15 || rg_Ftype6LastPkt == True ) begin
                          rg_TmpStreamDataDev16 <= {wr_DataPkt[127:96],96'd0};
				lv_Ftype6LastData = True;
						  $display("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@22");
						end
					else begin
								rg_TmpStreamDataDev16 <= 0;
				lv_Ftype6LastData = False; 
						end  */
	if (wr_Ftype6LastPkt == True) begin
				lv_Ftype6LastData = True;
                        wr_TmpStreamDataDev16_2 <= {rg_datapacket[31:0],wr_DataPkt[127:32]};
				end

    			else 
				lv_Ftype6LastData = False; 

		      	if (rg_Ftype6PktCount == 4'd0) 
                            lv_Ftype6Data = 0;
                        else if (rg_Ftype6PktCount == 4'd2) begin
                            lv_Ftype6Data = lv_TempStreamDataDev16;
                            rg_StreamData <= tagged Valid Ftype6StreamData {ftype6LastData: lv_Ftype6LastData, ftype6Data: lv_Ftype6Data};

                        end 
                        else if(wr_PktCount > 4'd2 && rg_Ftype6LastPkt == False) begin
                            lv_Ftype6Data = rg_TmpStreamDataDev16; 
		            rg_StreamData <= tagged Valid Ftype6StreamData {ftype6LastData: lv_Ftype6LastData, ftype6Data: lv_Ftype6Data};

                        end
else if (rg_Ftype6LastPkt == True) begin
lv_Ftype6Data = wr_TmpStreamDataDev16_2;
		            rg_StreamData <= tagged Valid Ftype6StreamData {ftype6LastData: lv_Ftype6LastData, ftype6Data: lv_Ftype6Data};

end


    		rg_Ftype6_StreamWrClass <= tagged Valid fn_Dev16Ftype6StreamPktHeader (wr_PktCount, rg_HeaderPktFtype6);
    	end
  	else begin
		rg_Ftype6_StreamWrClass <= tagged Invalid;
		rg_StreamData <= tagged Invalid;  
		rg_Ftype6LastData <= False;
                rg_TmpStreamDataDev16 <= 0; 
        end
//end

endrule

rule display2;
	$display("\n packet count == %h ",wr_PktCount);

endrule

// Module Definition 
 // Input Ports as Methods
 method Action _PktParseRx_SOF_n (Bool value); 
	pkt_Separation._inputs_SOF (!value);
 endmethod
 method Action _PktParseRx_EOF_n (Bool value);
	pkt_Separation._inputs_EOF (!value);
 endmethod
 method Action _PktParseRx_VLD_n (Bool value);
	pkt_Separation._inputs_VLD (!value);
 endmethod
/* method Bool link_rx_rdy_n_ (); // Need to Implement
	return (wr_TxReady_In); 
 endmethod*/

 method Action _PktParseRx_data (DataPkt value);
	pkt_Separation._inputs_DataPkt (value);
 endmethod

 method Action _PktParseRx_rem (Bit#(4) value); // Need to Implement
	wr_PktParseRx_rem <= value; 
 endmethod

 method Action _PktParseRx_crf (Bool value); // Need to Implement
	wr_PktParseRx_crf <= value; 
 endmethod

 method Action _inputs_TxReadyIn_From_Analyze (Bool value);
	wr_TxReady_In <= value; 
 endmethod 

 // Output Methods

 method Maybe#(FType6_StreamWrClass) outputs_RxFtype6StreamClass_ ();
	return rg_Ftype6_StreamWrClass;
 endmethod
 method Maybe#(Ftype6StreamData) outputs_RxFtype6StreamData_ ();
	return rg_StreamData;
 endmethod
/*method Maybe#(Data) outputs_RxFtype6StreamData_ ();
	return rg_StreamData;
 endmethod */
 method ReceivedPktsInfo outputs_ReceivedPkts_ ();
	return ReceivedPktsInfo {headerpkt: rg_HeaderPkt, datapkt: rg_DataPkt, pktcount: rg_PktCount, lastpkt: rg_LastPkt };
 endmethod
 
 method TT outputs_TTReceived_ ();
	return rg_TTReceived; 
 endmethod
 method DestId outputs_RxDestId_ ();
	return rg_DestIDReceived;
 endmethod
 method SourceId outputs_RxSourceId_ ();
	return rg_SrcIDReceived;
 endmethod
 method Prio outputs_RxPrioField_ ();
	return rg_PrioReceived;
 endmethod
 method Bit#(4) outputs_MaxPktCount_ ();
	return pkt_Separation.outputs_MaxPktCount_ ();
 endmethod


endmodule : mkRapidIO_PktTransportParse

endpackage : RapidIO_PktTransportParse
