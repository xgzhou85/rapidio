package Tb_ft81;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;

module mkTb_ft81(Empty);

	Ifc_InitiatorReqSignals initreq <- mkRapidIO_InitiatorReqIFC;
	Ifc_RapidIO_IOPktConcatenation pktcon <- mkRapidIO_IOPktConcatenation;
	Ifc_RapidIO_IOPkt_Generation gen <- mkRapidIO_IOPkt_Generation;
	Ifc_MaintenanceRespSignals main <- mkRapidIO_MaintenanceRespIFC;

	Wire#(InitReqDataInput) wr_data_count <- mkDWire(defaultValue);

	Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);

	rule rl_ref_clk_disp;
		reg_ref_clk <= reg_ref_clk + 1;
		$display (" \n----------------- CLOCK == %d ----------------------", reg_ref_clk);
		if (reg_ref_clk == 4)
		$finish (0);
	endrule

// ******************** FType 8 - Write response *************************
/*
  rule r1_ft8(reg_ref_clk==0);
	initreq._InitReqIfc._ireq_sof_n(False);
	initreq._InitReqIfc._ireq_eof_n(False);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'hffffffffffff11111111111111111111);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b1000);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0011);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule
*/
  rule r11_ft8(reg_ref_clk==0);
	main._MaintainRespIFC._mresp_sof_n(False);
	main._MaintainRespIFC._mresp_eof_n(False);
	main._MaintainRespIFC._mresp_vld_n(False);
	main._MaintainRespIFC._mresp_tt(2'b00);
	main._MaintainRespIFC._mresp_data(128'h0);
	main._MaintainRespIFC._mresp_crf(False);
	main._MaintainRespIFC._mresp_prio(2'b00);
	main._MaintainRespIFC._mresp_ftype(4'b1000);
	main._MaintainRespIFC._mresp_ttype(4'b0011);
	main._MaintainRespIFC._mresp_dest_id(32'h83990273);
	main._MaintainRespIFC._mresp_hop_count(0);
	main._MaintainRespIFC._mresp_local(False);
	main._MaintainRespIFC._mresp_status(0);
  endrule

   rule ready1;
	gen.pkgen_rdy_n (False);
  endrule

  rule ready2;
	pktcon._inputs_RxReady_From_IOGeneration(gen.outputs_RxRdy_From_Dest_());
  endrule

  rule ready3;
	initreq._inputs_IreqRDYIn_From_Concat(pktcon.outputs_RxRdy_From_Concat_());
  endrule
  
  rule ready4;
	main._inputs_MRespRDYIn_From_Concat(pktcon.outputs_RxRdy_From_Concat_());
  endrule

  rule r2_ft8;
	$display("Initiator request packet = %b",initreq.outputs_InitReqIfcPkt_ ());
	pktcon._inputs_InitReqIfcPkt(initreq.outputs_InitReqIfcPkt_ ());
	pktcon._inputs_MaintenanceIfcPkt(main.outputs_MaintainRespIfcPkt_ ());
  endrule

  rule r3_ft8;
	$display("FType 8 packet = %b",pktcon.outputs_Ftype8_IOMaintenancePacket_ ());
	gen._inputs_Ftype8IOMaintenanceClass(pktcon.outputs_Ftype8_IOMaintenancePacket_ ());
	gen._inputs_InitReqIfcPkt(pktcon.outputs_InitReqIfcPkt_ ());
  endrule

  rule r4_ft8;
	gen._inputs_InitReqDataCount(wr_data_count);
  endrule

  rule r5_ft8_genout;
	$display("Generation Module Outputs \n");
	$display("SOF = %b \n ",gen.pkgen_sof_n_ ());
	$display("EOF = %b \n ",gen.pkgen_eof_n_ ());
	$display("VLD = %b \n",gen.pkgen_vld_n_ ());
	$display("DSC = %b \n",gen.pkgen_dsc_n_ ());
	$display("TX_REM = %b \n",gen.pkgen_tx_rem_ ());
	$display("CRF = %b \n",gen.pkgen_crf_ ());
	$display("Data Packet = %h \n",gen.pkgen_data_());
	$display("RxRdy = %b",gen.outputs_RxRdy_From_Dest_());
  endrule


endmodule
endpackage





















