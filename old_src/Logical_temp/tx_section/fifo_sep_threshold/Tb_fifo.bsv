package Tb_fifo;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;
import fifo::*;

module mkTb_fifo(Empty);

	Ifc_InitiatorReqSignals initreq <- mkRapidIO_InitiatorReqIFC;
	Ifc_RapidIO_IOPktConcatenation pktcon <- mkRapidIO_IOPktConcatenation;
	Ifc_RapidIO_IOPkt_Generation gen <- mkRapidIO_IOPkt_Generation;
	Ifc_fifo fifo <- mkfifo;

	Wire#(InitReqDataInput) wr_data_count <- mkDWire(defaultValue);

	Reg#(Bit#(5)) reg_ref_clk <- mkReg (0);

	rule rl_ref_clk_disp;
		reg_ref_clk <= reg_ref_clk + 1;
		$display (" \n----------------- CLOCK == %d ----------------------", reg_ref_clk);
		if (reg_ref_clk == 17)
		$finish (0);
	endrule




// **************************************************************************

 rule r1_ft6(reg_ref_clk==0);
	initreq._InitReqIfc._ireq_sof_n(False);
	initreq._InitReqIfc._ireq_eof_n(True);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'hffffffffffff11111111111111111111);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b0110);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule

  
  rule r6_ft6(reg_ref_clk == 1 );
	initreq._InitReqIfc._ireq_sof_n(True);
	initreq._InitReqIfc._ireq_eof_n(True);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h11111111111122222222222222222222);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b0110);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule

  rule r11_ft236(reg_ref_clk== 2 );
	initreq._InitReqIfc._ireq_sof_n(True);
	initreq._InitReqIfc._ireq_eof_n(True);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h22222222222233333333333333333333);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b0110);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule

  rule r17_ft116(reg_ref_clk== 3 );
	initreq._InitReqIfc._ireq_sof_n(True);
	initreq._InitReqIfc._ireq_eof_n(True);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h33333333333344444444444444444444);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b0110);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule

rule r172_ft6776(reg_ref_clk== 4 );
	initreq._InitReqIfc._ireq_sof_n(True);
	initreq._InitReqIfc._ireq_eof_n(True);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h44444444444455555555555555555555);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b0110);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule

rule r17_ft656(reg_ref_clk== 5);
	initreq._InitReqIfc._ireq_sof_n(True);
	initreq._InitReqIfc._ireq_eof_n(True);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h55555555555566666666666666666666);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b0110);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule

rule r17_ft677(reg_ref_clk== 6 );
	initreq._InitReqIfc._ireq_sof_n(True);
	initreq._InitReqIfc._ireq_eof_n(True);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h66666666666677777777777777777777);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b0110);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule

rule r17_ft6777(reg_ref_clk== 7);
	initreq._InitReqIfc._ireq_sof_n(True);
	initreq._InitReqIfc._ireq_eof_n(True);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h77777777777788888888888888888888);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b0110);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule



rule r17_ft697(reg_ref_clk== 8);
	initreq._InitReqIfc._ireq_sof_n(True);
	initreq._InitReqIfc._ireq_eof_n(False);
	initreq._InitReqIfc._ireq_vld_n(False);
	initreq._InitReqIfc._ireq_dsc_n(True);
	initreq._InitReqIfc._ireq_tt(2'b00);
	initreq._InitReqIfc._ireq_data(128'h88888888888899999999999999999999);
	initreq._InitReqIfc._ireq_crf(False);
	initreq._InitReqIfc._ireq_prio(2'b01);
	initreq._InitReqIfc._ireq_ftype(4'b0110);
	initreq._InitReqIfc._ireq_dest_id(32'h83990273);
	initreq._InitReqIfc._ireq_addr(50'h000000008);
	initreq._InitReqIfc._ireq_hopcount(0);
	initreq._InitReqIfc._ireq_tid(8'hf2);
	initreq._InitReqIfc._ireq_ttype(4'b0000);
	initreq._InitReqIfc._ireq_byte_count(0);
	initreq._InitReqIfc._ireq_byte_en_n(0);		
	initreq._InitReqIfc._ireq_local(False);
	initreq._InitReqIfc._ireq_db_info(0);
	initreq._InitReqIfc._ireq_msg_len(0);
	initreq._InitReqIfc._ireq_msg_seg(0);
	initreq._InitReqIfc._ireq_mbox(0);
	initreq._InitReqIfc._ireq_letter(0);	
  endrule

rule ready1;
	gen.pkgen_rdy_n(fifo.outputs_RxRdy_From_Dest_());
  endrule

  rule ready2;
	pktcon._inputs_RxReady_From_IOGeneration(gen.outputs_RxRdy_From_Dest_());
  endrule

 rule ready3;
	initreq._inputs_IreqRDYIn_From_Concat(pktcon.outputs_RxRdy_From_Concat_());
  endrule

rule ready4;
	fifo.rdy_n (False);
endrule

  rule r2_ft6;
	$display("Initiator request packet = %b",initreq.outputs_InitReqIfcPkt_ ());
	pktcon._inputs_InitReqIfcPkt(initreq.outputs_InitReqIfcPkt_ ());
  endrule

  rule r3_ft6;
$display("FType 6 packet = %b",pktcon.outputs_Ftype6_IOStreamWrClassPacket_ ());
	gen._inputs_Ftype6IOStreamClass(pktcon.outputs_Ftype6_IOStreamWrClassPacket_ ());
	gen._inputs_InitReqIfcPkt(pktcon.outputs_InitReqIfcPkt_ ());
//fifo._input_transmit_pkt(gen.pkt_data_ ());
  endrule

rule fifo_in;
//gen._inputs_InitReqIfcPkt(pktcon.outputs_InitReqIfcPkt_ ());
fifo._input_transmit_pkt(gen.pkt_data_ ());
endrule

  rule r4_ft6;

	gen._inputs_InitReqDataCount(wr_data_count);
  endrule

  rule r5_ft6_genout;
	$display("Generation Module Outputs \n");
	$display("SOF = %b \n ",fifo.sof_n_ ());
	$display("EOF = %b \n ",fifo.eof_n_ ());
	$display("VLD = %b \n",fifo.vld_n_ ());
	$display("DSC = %b \n",fifo.dsc_n_ ());
	$display("TX_REM = %b \n",fifo.tx_rem_ ());
	$display("CRF = %b \n",fifo.crf_ ());
	$display("Data Packet = %h \n",fifo.data_());
	$display("RxRdy = %b",fifo.outputs_RxRdy_From_Dest_());
  endrule



endmodule
endpackage
