/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Target Request Operations Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This module is designed to perform the Target Resquest operations. 
-- Input to this module is Target Request Signals. Output of this module is Target Response 
-- Signals.
-- This module connects to Atomic Read/Write module to perform Atomic Read/Write operations.
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_TgtOperations;

import RapidIO_DTypes ::*;
import RapidIO_AtomicRdWrOp ::*; // Importing Atomic Read/Write module 

/*
-- Input Interface - Target Request Signals
-- Output Interface - Target Response Signals
*/
interface Ifc_RapidIO_TgtOperations;

// Input Methods (Target Request Signals)
 //-- Control Signal Interface
 method Action inputs_treq_sof (Bool value);
 method Action inputs_treq_eof (Bool value);
 method Action inputs_treq_vld (Bool value);
 method Bool outputs_treq_rdy_n ();

 //-- Data Signal Interface
 method Action inputs_treq_tt (TT value);
 method Action inputs_treq_data (Data value);
 method Action inputs_treq_crf (Bool value);
 method Action inputs_treq_prio (Prio value);
 method Action inputs_treq_ftype (Type value);
 method Action inputs_treq_dest_id (DestId value);
 method Action inputs_treq_source_id (SourceId value);
 method Action inputs_treq_tid (TranId value);
 method Action inputs_treq_ttype (Type value);
 method Action inputs_treq_addr (Addr value);
 method Action inputs_treq_byte_count (ByteCount value);
 method Action inputs_treq_byte_en (ByteEn value);

 //-- Message Signal Interface
 method Action inputs_treq_db_info (DoorBell value);
 method Action inputs_treq_msg_len (MsgLen value);
 method Action inputs_treq_msg_seg (MsgSeg value);
 method Action inputs_treq_mbox (Bit#(6) value);
 method Action inputs_treq_letter (Mletter value);

// Output Methods (Target Response Signals)
  //-- Control Signal Interface
 method Bool outputs_tresp_sof_ ();
 method Bool outputs_tresp_eof_ (); 
 method Bool outputs_tresp_vld_ ();
 method Bool outputs_tresp_dsc_ ();
 method Action _inputs_tresp_rdy_n_ (Bool value); // 

 //-- Data Signal Interface
 method TT outputs_tresp_tt_ ();
 method Data outputs_tresp_data_ ();
 method Bool outputs_tresp_crf_ ();
 method Prio outputs_tresp_prio_ ();
 method Type outputs_tresp_ftype_ ();
 method DestId outputs_tresp_dest_id_ ();
 method Status outputs_tresp_status_ ();
 method TranId outputs_tresp_tid_ ();
 method Type outputs_tresp_ttype_ ();
 method Bool outputs_tresp_no_data_ ();

 //-- Message Signal Interface
 method MsgSeg outputs_tresp_msg_seg_ ();
 method Bit#(2) outputs_tresp_mbox_ ();
 method Mletter outputs_tresp_letter ();

endinterface : Ifc_RapidIO_TgtOperations


(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_TgtOperations (Ifc_RapidIO_TgtOperations);

// Input Methods as Wires 
 //-- Control Signal Interface
Wire#(Bool) wr_treq_sof <- mkDWire (False);
Wire#(Bool) wr_treq_eof <- mkDWire (False);
Wire#(Bool) wr_treq_vld <- mkDWire (False);
Wire#(Bool) wr_treq_dsc <- mkDWire (False);
Wire#(Bool) wr_tresp_rdy <- mkDWire (False);

 //-- Data Signal Interface
Wire#(TT) wr_treq_tt <- mkDWire (0);
Wire#(Data) wr_treq_data <- mkDWire (0);
Wire#(Bool) wr_treq_crf <- mkDWire (False);
Wire#(Prio) wr_treq_prio <- mkDWire (0);
Wire#(Type) wr_treq_ftype <- mkDWire (0);
Wire#(DestId) wr_treq_dest_id <- mkDWire (0);
Wire#(SourceId) wr_treq_source_id <- mkDWire (0);
Wire#(TranId) wr_treq_tid <- mkDWire (0);
Wire#(Type) wr_treq_ttype <- mkDWire (0);
Wire#(Addr) wr_treq_addr <- mkDWire (0);
Wire#(ByteCount) wr_treq_byte_count <- mkDWire (0);
Wire#(ByteEn) wr_treq_byte_en <- mkDWire (0);

 //-- Message Signal Interface
Wire#(DoorBell) wr_treq_db_info <- mkDWire (0);
Wire#(MsgLen) wr_treq_msg_len <- mkDWire (0);
Wire#(MsgSeg) wr_treq_msg_seg <- mkDWire (0);
Wire#(Bit#(6)) wr_treq_mbox <- mkDWire (0);
Wire#(Mletter) wr_treq_letter <- mkDWire (0);

// Internal Wires and Registers 
Reg#(Bool) rg_treq_sof <- mkReg (False);
Reg#(Bool) rg_treq_eof <- mkReg (False);
Reg#(Bool) rg_treq_vld <- mkReg (False);
Reg#(Data) rg_treq_data <- mkReg (0);
Reg#(Addr) rg_treq_addr <- mkReg (0);
Reg#(Type) rg_treq_ftype <- mkReg (0);
Reg#(Type) rg_treq_ttype <- mkReg (0);
Reg#(SourceId) rg_treq_srcid <- mkReg (0);
Reg#(DestId) rg_treq_destid <- mkReg (0);
Reg#(TranId) rg_treq_TID <- mkReg (0);
Reg#(ByteCount) rg_treq_bytecount <- mkReg (0);
Reg#(ByteEn) rg_treq_byteen <- mkReg (0);
Wire#(Bool) wr_atomic_sof <- mkDWire (False);
Wire#(Bool) wr_atomic_valid <- mkDWire (False);

// Module Instantiation 
Ifc_RapidIO_AtomicRdWrOp rio_AtomicRdWrOpns <- mkRapidIO_AtomicRdWrOp (); 

// -- Rules -- 
/*
-- Following rule, It is used to connect input signals to Atomic Read/Write module to perform
-- Atomic Read operations
*/
rule rl_AtomicReadInput (wr_treq_ftype == 'd2);
    rio_AtomicRdWrOpns._inputs_treq_sof (wr_treq_sof);
    rio_AtomicRdWrOpns._inputs_treq_eof (wr_treq_eof);
    rio_AtomicRdWrOpns._inputs_treq_vld (wr_treq_vld);

    rio_AtomicRdWrOpns._inputs_treq_tt (wr_treq_tt);
    rio_AtomicRdWrOpns._inputs_treq_data (wr_treq_data);
    rio_AtomicRdWrOpns._inputs_treq_crf (wr_treq_crf);
    rio_AtomicRdWrOpns._inputs_treq_prio (wr_treq_prio);
    rio_AtomicRdWrOpns._inputs_treq_ftype (wr_treq_ftype);
    rio_AtomicRdWrOpns._inputs_treq_dest_id (wr_treq_dest_id);
    rio_AtomicRdWrOpns._inputs_treq_source_id (wr_treq_source_id);
    rio_AtomicRdWrOpns._inputs_treq_tid (wr_treq_tid);
    rio_AtomicRdWrOpns._inputs_treq_ttype (wr_treq_ttype);
    rio_AtomicRdWrOpns._inputs_treq_addr (wr_treq_addr);
    rio_AtomicRdWrOpns._inputs_treq_byte_count (wr_treq_byte_count);
    rio_AtomicRdWrOpns._inputs_treq_byte_en (wr_treq_byte_en);
endrule 

/*
-- Following rule, It is used to connect input signals to Atomic Read/Write module to perform
-- Atomic Write operations
*/
rule rl_AtomicWriteInput (wr_treq_ftype == 'd5);
    rio_AtomicRdWrOpns._inputs_treq_sof (wr_treq_sof);
    rio_AtomicRdWrOpns._inputs_treq_eof (wr_treq_eof);
    rio_AtomicRdWrOpns._inputs_treq_vld (wr_treq_vld);

    rio_AtomicRdWrOpns._inputs_treq_tt (wr_treq_tt);
    rio_AtomicRdWrOpns._inputs_treq_data (wr_treq_data);
    rio_AtomicRdWrOpns._inputs_treq_crf (wr_treq_crf);
    rio_AtomicRdWrOpns._inputs_treq_prio (wr_treq_prio);
    rio_AtomicRdWrOpns._inputs_treq_ftype (wr_treq_ftype);
    rio_AtomicRdWrOpns._inputs_treq_dest_id (wr_treq_dest_id);
    rio_AtomicRdWrOpns._inputs_treq_source_id (wr_treq_source_id);
    rio_AtomicRdWrOpns._inputs_treq_tid (wr_treq_tid);
    rio_AtomicRdWrOpns._inputs_treq_ttype (wr_treq_ttype);
    rio_AtomicRdWrOpns._inputs_treq_addr (wr_treq_addr);
    rio_AtomicRdWrOpns._inputs_treq_byte_count (wr_treq_byte_count);
    rio_AtomicRdWrOpns._inputs_treq_byte_en (wr_treq_byte_en);
endrule 

/*
-- Following rule is designed to monitor the atomic Read/Write output. 
-- It validates the output of Atomic Read/Write operations. 
*/
rule rl_Validating_Atomic_operations;
    wr_atomic_valid <= rio_AtomicRdWrOpns.outputs_tresp_vld_ ();
    wr_atomic_sof <= rio_AtomicRdWrOpns.outputs_tresp_sof_ ();
endrule 


// Input and Methods Definitions
// Input Methods (Target Request Signals)
 method Action inputs_treq_sof (Bool value);
	wr_treq_sof <= !value; 
 endmethod 
 method Action inputs_treq_eof (Bool value);
 	wr_treq_eof <= (!value); 
 endmethod 
 method Action inputs_treq_vld (Bool value);
	wr_treq_vld <= !value; 
 endmethod 
 method Bool outputs_treq_rdy_n ();
	return !(True);
 endmethod 

 method Action inputs_treq_tt (TT value);
	wr_treq_tt <= value; 
 endmethod 
 method Action inputs_treq_data (Data value);
	wr_treq_data <= value; 
 endmethod 
 method Action inputs_treq_crf (Bool value);
	wr_treq_crf <= value; 
 endmethod
 method Action inputs_treq_prio (Prio value);
	wr_treq_prio <= value; 
 endmethod 
 method Action inputs_treq_ftype (Type value);
	wr_treq_ftype <= value; 
 endmethod 
 method Action inputs_treq_dest_id (DestId value);
	wr_treq_dest_id <= value; 
 endmethod 
 method Action inputs_treq_source_id (SourceId value);
	wr_treq_source_id <= value;
 endmethod 
 method Action inputs_treq_tid (TranId value);
	wr_treq_tid <= value; 
 endmethod 
 method Action inputs_treq_ttype (Type value);
	wr_treq_ttype <= value; 
 endmethod 
 method Action inputs_treq_addr (Addr value);
	wr_treq_addr <= value; 
 endmethod 
 method Action inputs_treq_byte_count (ByteCount value);
	wr_treq_byte_count <= value; 
 endmethod 
 method Action inputs_treq_byte_en (ByteEn value);
	wr_treq_byte_en <= value; 
 endmethod 

 method Action inputs_treq_db_info (DoorBell value);
	wr_treq_db_info <= value; 
 endmethod 
 method Action inputs_treq_msg_len (MsgLen value);
	wr_treq_msg_len <= value; 
 endmethod 
 method Action inputs_treq_msg_seg (MsgSeg value);
	wr_treq_msg_seg <= value; 
 endmethod 
 method Action inputs_treq_mbox (Bit#(6) value);
	wr_treq_mbox <= value; 
 endmethod 
 method Action inputs_treq_letter (Mletter value);
	wr_treq_letter <= value; 
 endmethod 

/*
-- When both the Atomic Valid and SOF signals are Valid, Output of the Atomic Operations are Valid. 
*/


// Output Methods (Target Response Signals)
  //-- Control Signal Interface
 method Bool outputs_tresp_sof_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return !(rio_AtomicRdWrOpns.outputs_tresp_sof_ ());
   else 
	return True; 
 endmethod 
 method Bool outputs_tresp_eof_ (); 
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return !(rio_AtomicRdWrOpns.outputs_tresp_eof_ ()); 
   else 
	return True; 
 endmethod 
 method Bool outputs_tresp_vld_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return !(rio_AtomicRdWrOpns.outputs_tresp_vld_ ());
   else	
	return True; 
 endmethod 
 method Bool outputs_tresp_dsc_ ();
	return True; 
 endmethod 
 method Action _inputs_tresp_rdy_n_ (Bool value); // 
	wr_tresp_rdy <= !(value); 
 endmethod 

 //-- Data Signal Interface
 method TT outputs_tresp_tt_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return rio_AtomicRdWrOpns.outputs_tresp_tt_ ();
   else 
	return 0;
 endmethod 
 method Data outputs_tresp_data_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return rio_AtomicRdWrOpns.outputs_tresp_data_ ();
   else 
	return 0; 
 endmethod
 method Bool outputs_tresp_crf_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return rio_AtomicRdWrOpns.outputs_tresp_crf_ ();
   else
	return False; 
 endmethod 
 method Prio outputs_tresp_prio_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return rio_AtomicRdWrOpns.outputs_tresp_prio_ ();
   else 
	return 0; 
 endmethod 
 method Type outputs_tresp_ftype_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return rio_AtomicRdWrOpns.outputs_tresp_ftype_ ();
   else 
	return 0; 
 endmethod 
 method DestId outputs_tresp_dest_id_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return rio_AtomicRdWrOpns.outputs_tresp_dest_id_ ();
   else 
	return 0;
 endmethod 
 method Status outputs_tresp_status_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return rio_AtomicRdWrOpns.outputs_tresp_status_ ();
   else 
	return 0;
 endmethod 
 method TranId outputs_tresp_tid_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return rio_AtomicRdWrOpns.outputs_tresp_tid_ ();
   else 
	return 0; 
 endmethod 
 method Type outputs_tresp_ttype_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return rio_AtomicRdWrOpns.outputs_tresp_ttype_ ();
   else 
	return 0; 
 endmethod 
 method Bool outputs_tresp_no_data_ ();
   if ((wr_atomic_sof == True) && (wr_atomic_valid == True))
	return False;
   else 
	return False; 
 endmethod 

 //-- Message Signal Interface
 method MsgSeg outputs_tresp_msg_seg_ ();
	return 0;
 endmethod 
 method Bit#(2) outputs_tresp_mbox_ ();
	return 0;
 endmethod 
 method Mletter outputs_tresp_letter ();
	return 0; 
 endmethod	
endmodule : mkRapidIO_TgtOperations

endpackage : RapidIO_TgtOperations

