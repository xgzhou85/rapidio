/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Receive Ftype Functions Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module contains, 
-- 1. Functions require to parse the incoming packets and generate logical layer ftype packets.
-- 2.   
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_RxFTypeFunctionsDev8;

`include "RapidIO.defines"

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;

// Incoming Packet Decoding with respect to Ttype 
// -- Ftype 2
function FType2_RequestClass fn_Dev8Ftype2RequestPkt (Type ftype, Bit#(4) pktCount, DataPkt inHeaderPkt, DataPkt inDataPkt);
//    case (inHeaderPkt[39:36]) // ttype
//	'b0100: begin // NREAD 
		return FType2_RequestClass {
				tt: inHeaderPkt[125:124],
				ftype: inHeaderPkt[123:120],
				ttype: inHeaderPkt[103:100],
				rdsize: inHeaderPkt[99:96],
				srcTID: inHeaderPkt[95:88],
				addr: inHeaderPkt[87:59],
				wdptr: inHeaderPkt[58],
				xamsbs: inHeaderPkt[57:56] 
				};
//		end
//	default: return defaultValue;
//    endcase
endfunction

// -- Ftype 5
function FType5_WriteClass fn_Dev8Ftype5WritePkt (Type ftype, Bit#(4) pktCount, DataPkt inHeaderPkt, DataPkt inDataPkt, Bit#(29) addrReceived, Data dataReceived);
//    case (inHeaderPkt[39:36]) // ttype
//	'b0100: begin // NWRITE
		if (pktCount == 4'd2)
		    Data lv_Data = dataReceived;
		
                return FType5_WriteClass {
			tt: inHeaderPkt[125:124],
			ftype: inHeaderPkt[123:120],
			ttype: inHeaderPkt[103:100],
			wrsize: inHeaderPkt[99:96],
			srcTID: inHeaderPkt[95:88],
			addr: inHeaderPkt[87:59],
			wdptr: inHeaderPkt[58],
			xamsbs: inHeaderPkt[57:56],
			data : tagged Valid dataReceived
			};
//		end
/*
	'b0101: begin //NWRITE_R
		Bit#(29) lv_Addr = 0;
		Bit#(2) lv_xamsbs = 0;
		Bit#(1) lv_wdptr = 0;
		Data lv_Data = 0;
		if ((pktCount == 4'd2) || (pktCount == 4'd3)) begin
		    lv_Addr = addrReceived;
		    lv_xamsbs = inDataPkt[57:56];
		    lv_wdptr = inDataPkt[58];
		    end
		if (pktCount == 4'd3)
		    lv_Data = dataReceived;
		return FType5_WriteClass {
			ftype: inHeaderPkt[59:56],
			ttype: inHeaderPkt[39:36],
			wrsize: inHeaderPkt[35:32],
			srcTID: inHeaderPkt[31:24],
			addr: lv_Addr,
			wdptr: lv_wdptr,
			xamsbs: lv_xamsbs,
			data : tagged Valid lv_Data
			};
		end
*/
//	default : return defaultValue;
//    endcase
endfunction

// Ftype 6 
function FType6_StreamWrClass fn_Dev8Ftype6StreamPktHeader (Bit#(4) pktCount, DataPkt inHeaderPkt);
//    if (pktCount == 4'd1)
	return FType6_StreamWrClass {
				tt: inHeaderPkt[125:124],
				ftype: inHeaderPkt[123:120],
				addr: inHeaderPkt[103:75],
				xamsbs: inHeaderPkt[73:72]
				}; 
//    else 
//        return defaultValue; 
        
endfunction


// Ftype 8 
function FType8_MaintenanceClass fn_Dev8Ftype8MaintanenceRequestPkt (Bit#(4) pktCount, DataPkt inHeaderPkt, DataPkt inDataPkt);
    WdPointer lv_wdptr = 0;
    Offset lv_offset = 0;
    case (inHeaderPkt[103:100])
	'b0000: begin  // Maintenance Read Request 
		if (pktCount == 'd1) begin
			return FType8_MaintenanceClass {
					tt: inHeaderPkt[125:124],
					ftype: inHeaderPkt[123:120],
					ttype: inHeaderPkt[103:100],
					size: inHeaderPkt[99:96],
					tranID: inHeaderPkt[95:88],
					config_offset: inHeaderPkt[79:59],
					wdptr: inHeaderPkt[58],
					data: tagged Invalid
				};
		end
		else
			return defaultValue;
		end 
	'b0001: begin	// Maintenance Write Request
		return FType8_MaintenanceClass {
					tt: inHeaderPkt[125:124],
					ftype: inHeaderPkt[123:120],
					ttype: inHeaderPkt[103:100],
					size: inHeaderPkt[99:96],
					tranID: inHeaderPkt[95:88],
					config_offset: inHeaderPkt[79:59],
					wdptr: inHeaderPkt[58],
					data: tagged Invalid
				};
		end

	'b0010: begin   // Maintenance Read Response 
		return FType8_MaintenanceClass {
					tt: inHeaderPkt[125:124],
					ftype: inHeaderPkt[123:120],
					ttype: inHeaderPkt[103:100],
					size: inHeaderPkt[99:96],   // Status Message
					tranID: inHeaderPkt[95:88],
					config_offset: 0,
					wdptr: 0,
					data: tagged Invalid
				};
		end

	'b0011: begin   // Maintenance Write Response 
		return FType8_MaintenanceClass {
					tt: inHeaderPkt[125:124],
					ftype: inHeaderPkt[123:120],
					ttype: inHeaderPkt[103:100],
					size: inHeaderPkt[99:96],   // Status Message
					tranID: inHeaderPkt[95:88],
					config_offset: 0,
					wdptr: 0,
					data: tagged Invalid
				};
		end

	default: return defaultValue;
    endcase

endfunction

// Ftype 9
function FType9_DataStreamingClass fn_Dev8Ftype9logicalPkt (DataPkt inHeaderPkt, DataPkt inDataPkt, Bit#(4) pktcount);
    if(pktcount == 4'd1) begin
    return FType9_DataStreamingClass { 
					ftype: inHeaderPkt[115:112],
					cos: inHeaderPkt[95:88],
					start: inHeaderPkt[87:87],
					ends: inHeaderPkt[86:86],
					rsv: inHeaderPkt[85:83],
					xheader: inHeaderPkt[82:82],
					odd: inHeaderPkt[81:81],
					pad: inHeaderPkt[80:80],
					srl: inHeaderPkt[79:64],
				        data: inHeaderPkt[63:0]
				     };
			 end
    else begin
    return FType9_DataStreamingClass { 
ftype: inHeaderPkt[115:112],
					cos: inHeaderPkt[95:88],
					start: inHeaderPkt[87:87],
					ends: inHeaderPkt[86:86],
					rsv: inHeaderPkt[85:83],
					xheader: inHeaderPkt[82:82],
					odd: inHeaderPkt[81:81],
					pad: inHeaderPkt[80:80],
					srl: inHeaderPkt[79:64],
				        data: inDataPkt[127:64]
				     };
			 end
endfunction

// Ftype 11
function FType11_MESSAGEClass fn_Dev8Ftype11MessagePkt (Bit#(4) pktCount, DataPkt inHeaderPkt, DataPkt inDataPkt);
    return FType11_MESSAGEClass {
                                    tt: inHeaderPkt[125:124],
                                    ftype: inHeaderPkt[123:120],
		                    msglen: inHeaderPkt[103:100],
		                    ssize: inHeaderPkt[99:96],
		                    srcTID: inHeaderPkt[95:88],
		                    letter: inHeaderPkt[87:86],
		                    mbox: inHeaderPkt[85:84],
		                    msgseg: inHeaderPkt[83:80]
                                };
endfunction 

// Ftype 13
function FType13_ResponseClass fn_Dev8Ftype13ResponsePkt (Bit#(4) pktCount, DataPkt inHeaderPkt, DataPkt inDataPkt, Type ttype, Data respData);
    case (ttype) matches
	'd8 : if (pktCount == 'd1) // With Data
		return FType13_ResponseClass {		
				tt: inHeaderPkt[125:124],	
				ftype: inHeaderPkt[123:120],
				ttype: inHeaderPkt[103:100],
				status: inHeaderPkt[99:96],
				tgtTID: inHeaderPkt[95:88],
				data: tagged Valid respData
				};
	      else 
		return defaultValue;
    	'd0: if (pktCount == 'd1) // Without Data
		return FType13_ResponseClass {	
				tt: inHeaderPkt[125:124],		
				ftype: inHeaderPkt[123:120],
				ttype: inHeaderPkt[103:100],
				status: inHeaderPkt[99:96],
				tgtTID: inHeaderPkt[95:88],
				data: tagged Invalid
				};
	     else 
		return defaultValue;
    	default: return defaultValue;
    endcase
endfunction	

// Ftype 10 
function FType10_DOORBELLClass fn_Dev8Ftype10DoorBellPkt (Bit#(4) pktCount, DataPkt inHeaderPkt);
	return FType10_DOORBELLClass {	tt: inHeaderPkt[125:124],
					ftype: inHeaderPkt[123:120],
					srcTID: inHeaderPkt[95:88],
					info_msb: inHeaderPkt[87:80],
					info_lsb: inHeaderPkt[79:72]
					};
endfunction

endpackage : RapidIO_RxFTypeFunctionsDev8
