/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO InitEncoder_Wdptr_Size Testbench
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of the RapidIO_InitEncoder_WdPtr_Size. 
-- 1. All the cases for ByteCount is Checked.
-- 2. All conditions for ByteEn is Checked.
--
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_RapidIO_InitEncoder_WdPtr_Size;


import RapidIO_DTypes ::*;
import RapidIO_InitEncoder_WdPtr_Size::*;


module mkTb_for_RapidIO_InitEncoder_WdPtr_Size(Empty);

/// interface ////

Ifc_RapidIO_InitEncoder_WdPtr_Size  wdPtrSize <- mkRapidIO_InitEncoder_WdPtr_Size();


/// Wires are declared for output ////

Wire#(Size) wr_out_Size <- mkDWire(0);
Wire#(WdPointer) wr_out_wdPointer <- mkDWire(0);

//// 6 Bit Clock is given since 33  has to be compared with reg_ref_clk ///
Reg#(Bit#(6)) reg_ref_clk <- mkReg (0);		


///  clock  ////

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 34)
	$finish (0);
endrule

// In each clock diffent inputs are given to check all the conditions are met or not /////
// the interface for _inputs_Read is commented because it is  used only for checking 96,160,192 and 224 ///
//  Here both the read and write size are considered same for a particular bytecount and byteEn ///


rule rl_input0(reg_ref_clk == 0);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d1);
	wdPtrSize._inputs_ByteEn(8'b10000000);
endrule

rule rl_input1(reg_ref_clk == 1);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d1);
	wdPtrSize._inputs_ByteEn(8'b01000000);
endrule

rule rl_input2(reg_ref_clk == 2);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d1);
	wdPtrSize._inputs_ByteEn(8'b00100000);
endrule

rule rl_input3(reg_ref_clk == 3);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d1);
	wdPtrSize._inputs_ByteEn(8'b00010000);
endrule

rule rl_input4(reg_ref_clk == 4);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d1);
	wdPtrSize._inputs_ByteEn(8'b00001000);
endrule

rule rl_input5(reg_ref_clk == 5);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d1);
	wdPtrSize._inputs_ByteEn(8'b00000100);
endrule

rule rl_input6(reg_ref_clk == 6);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d1);
	wdPtrSize._inputs_ByteEn(8'b00000010);
endrule

rule rl_input7(reg_ref_clk == 7);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d1);
	wdPtrSize._inputs_ByteEn(8'b00000001);
endrule

rule rl_input8(reg_ref_clk == 8 );
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d2);
	wdPtrSize._inputs_ByteEn(8'b11000000);
endrule

rule rl_input9(reg_ref_clk == 9);
	wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d2);
	wdPtrSize._inputs_ByteEn(8'b00110000);
endrule

rule rl_input10(reg_ref_clk == 10);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d2);
	wdPtrSize._inputs_ByteEn(8'b00001100);
endrule

rule rl_input11(reg_ref_clk == 11);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d2);
	wdPtrSize._inputs_ByteEn(8'b00000011);
endrule

rule rl_input12(reg_ref_clk == 12);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d3);
	wdPtrSize._inputs_ByteEn(8'b11100000);
endrule

rule rl_input13(reg_ref_clk == 13);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d3);
	wdPtrSize._inputs_ByteEn(8'b00000111);
endrule

rule rl_input14(reg_ref_clk == 14);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d4);
	wdPtrSize._inputs_ByteEn(8'b11110000);
endrule

rule rl_input15(reg_ref_clk == 15);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d4);
	wdPtrSize._inputs_ByteEn(8'b00001111);
endrule

rule rl_input16(reg_ref_clk ==16 );
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d5);
	wdPtrSize._inputs_ByteEn(8'b11111000);
endrule

rule rl_input17(reg_ref_clk == 17);
	wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d5);
	wdPtrSize._inputs_ByteEn(8'b00011111);
endrule

rule rl_input18(reg_ref_clk == 18);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d6);
	wdPtrSize._inputs_ByteEn(8'b11111100);
endrule

rule rl_input19(reg_ref_clk == 19);
	wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d6);
	wdPtrSize._inputs_ByteEn(8'b00111111);
endrule

rule rl_input20(reg_ref_clk ==20 );
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d7);
	wdPtrSize._inputs_ByteEn(8'b11111110);
endrule

rule rl_input21(reg_ref_clk == 21);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d7);
	wdPtrSize._inputs_ByteEn(8'b01111111);
endrule

rule rl_input22(reg_ref_clk == 22);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d8);
	wdPtrSize._inputs_ByteEn(8'b11111111);
endrule

rule rl_input23(reg_ref_clk == 23);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d16);
endrule

rule rl_input24(reg_ref_clk == 24);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d32);
endrule

rule rl_input25(reg_ref_clk == 25);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d64);
endrule

rule rl_input26(reg_ref_clk == 26);
	wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d96);
endrule

rule rl_input27(reg_ref_clk == 27);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d128);
endrule

rule rl_input28(reg_ref_clk == 28);
	wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d160);
endrule

rule rl_input29(reg_ref_clk == 29);
	wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d192);
endrule

rule rl_input30(reg_ref_clk == 30);
	wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d224);
endrule

rule rl_input31(reg_ref_clk == 31);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d256);
endrule

// This rule is used to check for default value //

rule rl_input32(reg_ref_clk == 32);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d257);
endrule

rule rl_input33(reg_ref_clk == 33);
	//wdPtrSize._inputs_Read(True);
	wdPtrSize._inputs_ByteCount('d0);
endrule

// This rule is used for obtaining Output ( read size or write size and Wdpointer) ////
rule rl_output;
     	wr_out_Size<=wdPtrSize.outputs_Size_ ();  
     	wr_out_wdPointer<=wdPtrSize.outputs_WdPointer_ ();
endrule

rule rl_disp;
     $display("\n  size = %b , WdPointer =  %b  ",wr_out_Size,wr_out_wdPointer);
       
endrule

endmodule

endpackage
