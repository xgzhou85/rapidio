/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Delay Testbench
-- 
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of the RapidIO where a bit input is given which has to get after certain delay. 
-- 1. The delay is given as input while creating interface object.
-- 2. A delay of four units is provided.
-- 3. So after four clock pulse the output will be equal to the input which we have given four clock pulse before. 
-- 4. The input data to be delayed is given at rule rl_input. 

--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package Tb_RapidIO_Delay;

import RapidIO_Delay::*;

module mkTb_for_RapidIO_Delay();

// Module Instantiation for RapidIO_Delay
Ifc_RapidIO_Delay  riodelay <- mkRapidIO_Delay('b100);    // Delay of four units is provided //

Wire#(bit) wr_dout <- mkDWire(0);
Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);

/*
-- Following rule, it is used to generate reference clock 
*/
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 'b100)
	$finish (0);
endrule

/*
-- Following rule is used to provide input data 
*/
rule rl_input;
	riodelay._inputs('b1);
endrule

/*
-- Following rule is used to get back input data after certain delay
*/
rule rl_output;
     wr_dout<=riodelay.dout_();  
endrule

// rule rl_disp is used to display the delayed input signal //
rule rl_disp(reg_ref_clk == 'b100);
     $display("\n The delayed input signal %b",wr_dout);
       
endrule


endmodule
endpackage
