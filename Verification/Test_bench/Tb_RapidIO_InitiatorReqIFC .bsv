/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO_InitiatorReqIFC Testbench
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of the RapidIO_InitiatorReqIFC. 
-- Generation of Initiator Request signals 32 bit device, 16 bit device and 8 bit device
--  
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_RapidIO_InitiatorReqIFC;

import DefaultValue ::*;
import RapidIO_DTypes::*;
import RapidIO_InitEncoder_WdPtr_Size::*;
import RapidIO_InitiatorReqIFC::*;

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkTb_for_RapidIO_InitiatorReqIFC(Empty);

// interface

Ifc_InitiatorReqSignals  initreqsignals <- mkRapidIO_InitiatorReqIFC();

Wire#(Bool) wr_ready <- mkDWire (False); 
Wire#(InitiatorReqIfcPkt) wr_init_req_sig <- mkDWire (defaultValue);
Wire#(InitReqIfcCntrl) wr_control <- mkDWire (defaultValue);
Wire#(InitReqIfcData) wr_data <- mkDWire (defaultValue);
Wire#(InitReqIfcMsg) wr_msg <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_packet <- mkDWire (defaultValue);

// Clock Declaration

Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);		


//  clock

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 8)
	$finish (0);
endrule

// The device type is selected w.r.t. tt

rule rl_input0(reg_ref_clk == 0);
	$display ("\n \n For 32 bit device " );
	initreqsignals._InitReqIfc._ireq_sof_n(False);
	initreqsignals._InitReqIfc._ireq_eof_n(True);
	initreqsignals._InitReqIfc._ireq_vld_n(False);
	initreqsignals._InitReqIfc._ireq_dsc_n(True);
	initreqsignals._InitReqIfc._ireq_tt(2'b10);
	initreqsignals._InitReqIfc._ireq_data(64'h9999999999999999);
	initreqsignals._InitReqIfc._ireq_crf(False);
	initreqsignals._InitReqIfc._ireq_prio(2'b01);
	initreqsignals._InitReqIfc._ireq_ftype(4'b0101);
	initreqsignals._InitReqIfc._ireq_dest_id(32'hda34568c);
	initreqsignals._InitReqIfc._ireq_addr(50'h000000008);
	initreqsignals._InitReqIfc._ireq_hopcount(0);
	initreqsignals._InitReqIfc._ireq_tid(8'hbf);
	initreqsignals._InitReqIfc._ireq_ttype(4'b0100);
	initreqsignals._InitReqIfc._ireq_byte_count('d4);
	initreqsignals._InitReqIfc._ireq_byte_en_n(0);		// this field is not used since eof is not asserted //
	initreqsignals._InitReqIfc._ireq_local(False);
	initreqsignals._InitReqIfc._ireq_db_info(16'b0000111100001111);
	initreqsignals._InitReqIfc._ireq_msg_len(4'b0110);
	initreqsignals._InitReqIfc._ireq_msg_seg(4'b0010);
	initreqsignals._InitReqIfc._ireq_mbox(6'b001000);
	initreqsignals._InitReqIfc._ireq_letter(2'b10);
	initreqsignals._inputs_IreqRDYIn_From_Concat(False);
endrule

// This rule passes the corresponding values to wires for generating reference packet (line 303)

rule rl_in0(reg_ref_clk == 0);
	wr_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, ireq_destid:32'hda34568c, 			   ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:0, ireq_local:False};
	wr_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Eof is made active low so that packet is not generated

rule rl_input1(reg_ref_clk == 1);
	initreqsignals._InitReqIfc._ireq_sof_n(True);
	initreqsignals._InitReqIfc._ireq_eof_n(False);
	initreqsignals._InitReqIfc._ireq_vld_n(True);
	initreqsignals._InitReqIfc._ireq_dsc_n(False);
	initreqsignals._InitReqIfc._ireq_tt(2'b10);
	initreqsignals._InitReqIfc._ireq_data(64'h9999888899999999);
	initreqsignals._InitReqIfc._ireq_crf(False);
	initreqsignals._InitReqIfc._ireq_prio(0);		
	initreqsignals._InitReqIfc._ireq_ftype(4'b0101); 
	initreqsignals._InitReqIfc._ireq_dest_id(32'hda34568c);
	initreqsignals._InitReqIfc._ireq_addr(50'h000000008);
	initreqsignals._InitReqIfc._ireq_hopcount(0);
	initreqsignals._InitReqIfc._ireq_tid(8'hbf);			 
	initreqsignals._InitReqIfc._ireq_ttype(4'b0100);
	initreqsignals._InitReqIfc._ireq_byte_count('d4);
	initreqsignals._InitReqIfc._ireq_byte_en_n(8'h0f);	// this feild is considered since eof is asserted
	initreqsignals._InitReqIfc._ireq_local(False);
	initreqsignals._InitReqIfc._ireq_db_info(16'd23);
	initreqsignals._InitReqIfc._ireq_msg_len(4'b0110);
	initreqsignals._InitReqIfc._ireq_msg_seg(4'b0010);
	initreqsignals._InitReqIfc._ireq_mbox(6'b001000);
	initreqsignals._InitReqIfc._ireq_letter(2'b10);
	initreqsignals._inputs_IreqRDYIn_From_Concat(False);
endrule

// All values are made zero(default) except byte_en since eof is asserted

rule rl_in1(reg_ref_clk == 1);
	wr_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:True, ireq_vld:False, ireq_dsc:True};	
	wr_data <= InitReqIfcData {ireq_tt:0, ireq_data:0, ireq_crf:False, ireq_prio:0, ireq_ftype:0, ireq_destid:0,ireq_addr:0, ireq_hopcount:0, 			   ireq_tid:0,ireq_ttype:0, ireq_byte_count:0, ireq_byte_en:8'h0f, ireq_local:False};
	wr_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// 16 Bit Device //

rule rl_input2(reg_ref_clk == 2);
	$display ("\n \n For 16 bit device " );
	initreqsignals._InitReqIfc._ireq_sof_n(False);
	initreqsignals._InitReqIfc._ireq_eof_n(True);
	initreqsignals._InitReqIfc._ireq_vld_n(False);
	initreqsignals._InitReqIfc._ireq_dsc_n(True);
	initreqsignals._InitReqIfc._ireq_tt(2'b01);
	initreqsignals._InitReqIfc._ireq_data(64'h9999999999999999);
	initreqsignals._InitReqIfc._ireq_crf(False);
	initreqsignals._InitReqIfc._ireq_prio(2'b01);
	initreqsignals._InitReqIfc._ireq_ftype(4'b0101);
	initreqsignals._InitReqIfc._ireq_dest_id(32'hda8c0000);
	initreqsignals._InitReqIfc._ireq_addr(50'h000000008);
	initreqsignals._InitReqIfc._ireq_hopcount(0);
	initreqsignals._InitReqIfc._ireq_tid(8'hbf);
	initreqsignals._InitReqIfc._ireq_ttype(4'b0100);
	initreqsignals._InitReqIfc._ireq_byte_count('d4);
	initreqsignals._InitReqIfc._ireq_byte_en_n(0);			// this field is not used since eof is not asserted //
	initreqsignals._InitReqIfc._ireq_local(False);
	initreqsignals._InitReqIfc._ireq_db_info(16'b0000111100001111);
	initreqsignals._InitReqIfc._ireq_msg_len(4'b0110);
	initreqsignals._InitReqIfc._ireq_msg_seg(4'b0010);
	initreqsignals._InitReqIfc._ireq_mbox(6'b001000);
	initreqsignals._InitReqIfc._ireq_letter(2'b10);
	initreqsignals._inputs_IreqRDYIn_From_Concat(False);
endrule

// This rule passes the corresponding values to wires for generating reference packet (line 303)

rule rl_in2(reg_ref_clk == 2);
	wr_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_data <= InitReqIfcData {ireq_tt:2'b01, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, ireq_destid:32'hda8c0000, 			   ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:0, ireq_local:False};
	wr_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Eof is made active low so that packet is not generated //

rule rl_input3(reg_ref_clk == 3);
	initreqsignals._InitReqIfc._ireq_sof_n(True);
	initreqsignals._InitReqIfc._ireq_eof_n(False);
	initreqsignals._InitReqIfc._ireq_vld_n(True);
	initreqsignals._InitReqIfc._ireq_dsc_n(False);
	initreqsignals._InitReqIfc._ireq_tt(2'b01);
	initreqsignals._InitReqIfc._ireq_data(64'h9999888899999999);
	initreqsignals._InitReqIfc._ireq_crf(False);
	initreqsignals._InitReqIfc._ireq_prio(0);			
	initreqsignals._InitReqIfc._ireq_ftype(4'b0101);
	initreqsignals._InitReqIfc._ireq_dest_id(32'hda8c0000);
	initreqsignals._InitReqIfc._ireq_addr(50'h000000008);
	initreqsignals._InitReqIfc._ireq_hopcount(0);
	initreqsignals._InitReqIfc._ireq_tid(8'hbf);
	initreqsignals._InitReqIfc._ireq_ttype(4'b0100);
	initreqsignals._InitReqIfc._ireq_byte_count('d4);
	initreqsignals._InitReqIfc._ireq_byte_en_n(8'h0f);		// this feild is considered since eof is asserted
	initreqsignals._InitReqIfc._ireq_local(False);
	initreqsignals._InitReqIfc._ireq_db_info(16'd23);
	initreqsignals._InitReqIfc._ireq_msg_len(4'b0110);
	initreqsignals._InitReqIfc._ireq_msg_seg(4'b0010);
	initreqsignals._InitReqIfc._ireq_mbox(6'b001000);
	initreqsignals._InitReqIfc._ireq_letter(2'b10);
	initreqsignals._inputs_IreqRDYIn_From_Concat(False);
endrule

// All values are made zero(default) except byte_en since eof is asserted

rule rl_in3(reg_ref_clk == 3);
	wr_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:True, ireq_vld:False, ireq_dsc:True};	
	wr_data <= InitReqIfcData {ireq_tt:0, ireq_data:0, ireq_crf:False, ireq_prio:0, ireq_ftype:0, ireq_destid:0,ireq_addr:0, ireq_hopcount:0, 			   ireq_tid:0,ireq_ttype:0, ireq_byte_count:0, ireq_byte_en:8'h0f, ireq_local:False};
	wr_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// For 8 Bit Device 

rule rl_input4(reg_ref_clk == 4);
	$display ("\n \n For 8 bit device " );
	initreqsignals._InitReqIfc._ireq_sof_n(False);
	initreqsignals._InitReqIfc._ireq_eof_n(True);
	initreqsignals._InitReqIfc._ireq_vld_n(False);
	initreqsignals._InitReqIfc._ireq_dsc_n(True);
	initreqsignals._InitReqIfc._ireq_tt(2'b00);
	initreqsignals._InitReqIfc._ireq_data(64'h9999999999999999);
	initreqsignals._InitReqIfc._ireq_crf(False);
	initreqsignals._InitReqIfc._ireq_prio(2'b01);
	initreqsignals._InitReqIfc._ireq_ftype(4'b0101);
	initreqsignals._InitReqIfc._ireq_dest_id(32'hda000000);
	initreqsignals._InitReqIfc._ireq_addr(50'h000000008);
	initreqsignals._InitReqIfc._ireq_hopcount(0);
	initreqsignals._InitReqIfc._ireq_tid(8'hbf);
	initreqsignals._InitReqIfc._ireq_ttype(4'b0100);
	initreqsignals._InitReqIfc._ireq_byte_count('d4);
	initreqsignals._InitReqIfc._ireq_byte_en_n(0);			// this field is not used since eof is not asserted 
	initreqsignals._InitReqIfc._ireq_local(False);
	initreqsignals._InitReqIfc._ireq_db_info(16'b0000111100001111);
	initreqsignals._InitReqIfc._ireq_msg_len(4'b0110);
	initreqsignals._InitReqIfc._ireq_msg_seg(4'b0010);
	initreqsignals._InitReqIfc._ireq_mbox(6'b001000);
	initreqsignals._InitReqIfc._ireq_letter(2'b10);
	initreqsignals._inputs_IreqRDYIn_From_Concat(False);
endrule

// This rule passes the corresponding values to wires for generating reference packet (line 303)

rule rl_in4(reg_ref_clk == 4);
	wr_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, ireq_destid:32'hda000000, 			   ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:0, ireq_local:False};
	wr_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Eof is made active low so that packet is not generated //

rule rl_input5(reg_ref_clk == 5);
	initreqsignals._InitReqIfc._ireq_sof_n(True);
	initreqsignals._InitReqIfc._ireq_eof_n(False);
	initreqsignals._InitReqIfc._ireq_vld_n(True);
	initreqsignals._InitReqIfc._ireq_dsc_n(False);
	initreqsignals._InitReqIfc._ireq_tt(2'b00);
	initreqsignals._InitReqIfc._ireq_data(64'h9999888899999999);
	initreqsignals._InitReqIfc._ireq_crf(False);
	initreqsignals._InitReqIfc._ireq_prio(0);			
	initreqsignals._InitReqIfc._ireq_ftype(4'b0101);
	initreqsignals._InitReqIfc._ireq_dest_id(32'hda000000);
	initreqsignals._InitReqIfc._ireq_addr(50'h000000008);
	initreqsignals._InitReqIfc._ireq_hopcount(0);
	initreqsignals._InitReqIfc._ireq_tid(8'hbf);
	initreqsignals._InitReqIfc._ireq_ttype(4'b0100);
	initreqsignals._InitReqIfc._ireq_byte_count('d4);
	initreqsignals._InitReqIfc._ireq_byte_en_n(8'h0f);		// this feild is considered since eof is asserted
	initreqsignals._InitReqIfc._ireq_local(False);
	initreqsignals._InitReqIfc._ireq_db_info(16'd23);
	initreqsignals._InitReqIfc._ireq_msg_len(4'b0110);
	initreqsignals._InitReqIfc._ireq_msg_seg(4'b0010);
	initreqsignals._InitReqIfc._ireq_mbox(6'b001000);
	initreqsignals._InitReqIfc._ireq_letter(2'b10);
	initreqsignals._inputs_IreqRDYIn_From_Concat(False);
endrule

// All values are made zero(default) except byte_en since eof is asserted

rule rl_in5(reg_ref_clk == 5);
	wr_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:True, ireq_vld:False, ireq_dsc:True};	
	wr_data <= InitReqIfcData {ireq_tt:0, ireq_data:0, ireq_crf:False, ireq_prio:0, ireq_ftype:0, ireq_destid:0,ireq_addr:0, ireq_hopcount:0, 			   ireq_tid:0,ireq_ttype:0, ireq_byte_count:0, ireq_byte_en:8'h0f, ireq_local:False};
	wr_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule


rule rl_output;
     	wr_ready <= initreqsignals._InitReqIfc.ireq_rdy_n_ ();		//output from initiator request
     	wr_init_req_sig <= initreqsignals.outputs_InitReqIfcPkt_ ();	//output from initiator request
	wr_packet <= InitiatorReqIfcPkt {ireqcntrl:wr_control, ireqdata:wr_data, ireqmsg:wr_msg};	// reference packet
endrule

rule rl_disp;
     $display("\n  Ready = %b , \n reference packet =  %b \n  Initiator Packet =     %b ",wr_ready,wr_packet,wr_init_req_sig);
       
endrule

rule rl_compare;
	if(wr_packet == wr_init_req_sig)
	$display("\n  Initiator packet is equal to reference packet ");
	else
	$display("\n   Initiator packet is NOT equal to reference packet");	

endrule


endmodule

endpackage
