/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_TL_DRIVER_SVH
`define RAPIDIO_TL_DRIVER_SVH

// Callback defined for  Transport Layer agent
virtual class rapidio_tl_driver_callback extends uvm_callback;
  
  // Constructor for callback
  function new(string name = "rapidio_tl_driver_callback");
    super.new(name);
  endfunction:new
 
  // Tasks defined in callback
  virtual task tl_error();
  endtask

endclass: rapidio_tl_driver_callback

// Declaration of Driver Class
class rapidio_tl_driver extends uvm_driver #(rapidio_tl_sequence_item);
 
  virtual interface         rapidio_interface  vif;
  //SB ANALYSIS PORT ADDED HERE 
  uvm_analysis_port #(rapidio_tl_sequence_item)dr2sb_tl_port;
  uvm_analysis_port #(rapidio_tl_sequence_item)dr2sb_tl_dut_port;
  uvm_analysis_port #(rapidio_tl_sequence_item)dr2cov_tl_port;
  event packet2send_tl_driver;


  // Variable Declaration
  rapidio_tl_sequence_item req;
  rapidio_tl_sequence_item t1;
  rapidio_tl_sequence_item receive_pkt; // queues for storing the transactions
  rapidio_tl_agent_config tl_cfg;
  
//ADDITIONS
uvm_tlm_fifo #(byte unsigned)packet;
uvm_tlm_fifo #(byte unsigned)send_packet;

// Port from Logical layer
uvm_blocking_get_port #(byte unsigned) get_ll_pkt_port;
uvm_blocking_get_port #(int) get_ll_cntrl_port;

//To send packet to the next layer
uvm_blocking_put_port #(byte unsigned) packet_to_send;
uvm_blocking_put_port #(int) control_to_send;

 bit                          item_available = 0;  

  // UVM automation macros
  `uvm_component_utils_begin(rapidio_tl_driver)
    `uvm_field_object(req,UVM_ALL_ON)
    `uvm_field_object(receive_pkt,UVM_ALL_ON)
    `uvm_field_object(tl_cfg,UVM_ALL_ON)
    `uvm_field_int(item_available,UVM_ALL_ON)
  `uvm_component_utils_end

  // Register Callback with driver
   `uvm_register_cb(rapidio_tl_driver,rapidio_tl_driver_callback)
  // Task & Function declarations
   extern function new (string name, uvm_component parent);
   extern function void build_phase(uvm_phase phase);
   extern virtual task run_phase(uvm_phase phase);
   extern virtual task drive_reset_values();
   extern virtual task get_ll_pkt(rapidio_tl_sequence_item t1);
   extern virtual task send_req_pkt(rapidio_tl_sequence_item t1);

endclass: rapidio_tl_driver
  
// FUNC : Component constructor
function rapidio_tl_driver::new(string name, uvm_component parent); 
  super.new(name,parent);
  req = new();
  t1 = new();
  packet = new("packet",this);
  send_packet = new("send_packet",this);
  get_ll_pkt_port = new("get_ll_pkt_port",this);
  get_ll_cntrl_port = new("get_ll_cntrl_port",this);
  packet_to_send = new("packet_to_send",this);
  control_to_send = new("control_to_send",this);
  dr2sb_tl_port  =  new("dr2sb_tl_port",this);
  dr2sb_tl_dut_port  =  new("dr2sb_tl_dut_port",this);
  dr2cov_tl_port  =  new("dr2cov_tl_port",this);
endfunction: new

// FUNC : BUILD phase
function void rapidio_tl_driver::build_phase(uvm_phase phase);
  super.build_phase(phase);
  if(!uvm_config_db#(virtual rapidio_interface)::get(this, "", "vif", vif))
    `uvm_fatal("RAPIDIO_TL_DRVR_FATAL",{"VIRTUAL INTERFACE MUST BE SET FOR: ",get_full_name(),".vif"});
  void'(uvm_config_db#(rapidio_tl_agent_config)::get(this,"","rapidio_tl_agent_config",tl_cfg));

endfunction: build_phase

// TASK : RUN PHASE
task rapidio_tl_driver::run_phase(uvm_phase phase);
  super.run_phase(phase);
  drive_reset_values();
  //`uvm_info("RAPIDIO_TL_BFM_INFO",$sformatf("RAPIDIO_TL_BFM STARTED\n"),UVM_LOW);
  forever 
    begin
      //`uvm_info("RAPIDIO_TL_BFM_INFO",$sformatf("FOREVER STARTED\n"),UVM_LOW);
      seq_item_port.get_next_item(req);
      item_available = 1;
      get_ll_pkt(req);
      send_req_pkt(req); 
      seq_item_port.item_done();
    end    
endtask : run_phase

// TASK : get_ll_pkt
task rapidio_tl_driver::get_ll_pkt(rapidio_tl_sequence_item t1);
  int ll_pkt_len;
  byte unsigned ll_bytes;
  int i=0;
    get_ll_cntrl_port.get(ll_pkt_len);
   // `uvm_info("RAPIDIO_TL_BFM_INFO",$sformatf("LL_PKT_LEN =%d\n",ll_pkt_len),UVM_LOW);
    t1.ll_pkt_bytes = new[ll_pkt_len];
    for(i=0; i< ll_pkt_len; i++)
      begin 
        get_ll_pkt_port.get(ll_bytes);
        t1.ll_pkt_bytes[i] = ll_bytes;
        dr2sb_tl_port.write(t1);
        dr2sb_tl_dut_port.write(t1);
        dr2cov_tl_port.write(t1);
        ->packet2send_tl_driver;
      end 
endtask

// TASK : drive_reset_values - Drive reset values in the RAPIDIO interface
task rapidio_tl_driver::drive_reset_values();
  //`uvm_info("RAPIDIO_TL_DRV_INFO",$sformatf("DRIVING RESET VALUES ON TL LAYER I/F \n"), UVM_LOW);
endtask : drive_reset_values

//sending data packet and control info to the next layer
task rapidio_tl_driver::send_req_pkt(rapidio_tl_sequence_item t1);
  byte unsigned tl_bytes[];
  int tl_pkt_len;
  int tl_byte_size;
  //`uvm_info("RAPIDIO_TL_DRIVER_INFO",$sformatf("PL SEQITEM TO SEND  =%p \n",req ),UVM_LOW);
  tl_pkt_len = req.pack_bytes(tl_bytes);
  tl_byte_size = tl_bytes.size;
  tl_pkt_len =   tl_pkt_len/8;
  //`uvm_info("RAPIDIO_TL_BFM_INFO",$sformatf("TL_PKT_LEN =%d\n",tl_pkt_len),UVM_LOW);
  control_to_send.put(tl_pkt_len);
  foreach (tl_bytes[i]) 
    begin  
     packet_to_send.put(tl_bytes[i]);
    end
  item_available    = 1'b0; 
endtask


`endif






