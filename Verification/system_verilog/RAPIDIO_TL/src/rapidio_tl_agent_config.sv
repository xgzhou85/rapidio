/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_TL_AGENT_CONFIG_SVH
`define RAPIDIO_TL_AGENT_CONFIG_SVH

// Class Description  
class rapidio_tl_agent_config extends uvm_object;


string                   agent_name            = "TRANSPORT_LAYER_AGENT";
bit                      has_checks            = 1'b1;
bit                      has_coverage          = 1'b1;
uvm_active_passive_enum  is_active             = UVM_ACTIVE;
bit                      switch_based_topology = 1'b1;
bit                      ring_based_topology   = 1'b0; 
bit                      has_switches          = 1'b1;
bit                      is_endpoint           = 1'b0;
bit                      has_scoreboard        = 1'b0;

bit                      dev32_support         = 1'b0;
bit                      std_route_table_config_support = 1'b1;
bit                      dev16_support         = 1'b1;
bit [15:0]               max_destination_id;
bit [7:0]                dev8_base_device_id;
bit [15:0]               dev16_base_device_id;
bit [31:0]               dev32_base_device_id;
bit [15:0]               host_base_device_id;
bit [15:0]               host_base_device32_id;
bit [31:0]               component_tag;
bit                      ext_config_enable;

// UVM Automation macros
  `uvm_object_utils_begin(rapidio_tl_agent_config)
    `uvm_field_string(agent_name, UVM_ALL_ON)
    `uvm_field_int(has_checks, UVM_ALL_ON)
    `uvm_field_int(has_coverage, UVM_ALL_ON)
    `uvm_field_enum(uvm_active_passive_enum, is_active, UVM_ALL_ON)
    `uvm_field_int(switch_based_topology, UVM_ALL_ON)
    `uvm_field_int(ring_based_topology, UVM_ALL_ON)
    `uvm_field_int(has_switches, UVM_ALL_ON)
    `uvm_field_int(is_endpoint, UVM_ALL_ON)  
  `uvm_object_utils_end

// Standard UVM Methods

extern function new(string name = "rapidio_tl_agent_config");

endclass: rapidio_tl_agent_config 

function rapidio_tl_agent_config::new(string name = "rapidio_tl_agent_config");
  super.new(name);
endfunction


`endif

