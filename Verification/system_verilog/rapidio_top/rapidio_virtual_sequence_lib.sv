/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -------------------------------------------------------------------------------------------------------------------------------------------*/
class rapidio_nread_sequence extends virtual_sequence_base;

  
   rapidio_ll_nread_seq ll_nread;
   rapidio_8bit_read_write_seq tl_8bit_read_write;


  // Register the object in the factory //

   `uvm_object_utils(rapidio_nread_sequence)

  // Object consruction //

   function new(string name = "rapidio_nread_sequence");
   super.new(name);
   endfunction

  // Declaration of an user defined task called body() //

   task body();
     super.body; // Sets up the sub-sequencer pointers
     tl_8bit_read_write = rapidio_8bit_read_write_seq::type_id::create("tl_8bit_read_write");
     ll_nread= rapidio_ll_nread_seq::type_id::create("ll_nread");
   
      ll_nread.start(rio_ll_seqcr);
      tl_8bit_read_write.start(rio_tl_seqcr);
     

    // end
   endtask: body

endclass: rapidio_nread_sequence 


class rapidio_nwrite_sequence extends virtual_sequence_base;

  
   rapidio_ll_nwrite_seq ll_nwrite;
 //  rapidio_ll_nread_seq ll_nread;
   rapidio_8bit_read_write_seq tl_8bit_read_write;


  // Register the object in the factory //

   `uvm_object_utils(rapidio_nwrite_sequence)

  // Object consruction //

   function new(string name = "rapidio_nwrite_sequence");
   super.new(name);
   endfunction

  // Declaration of an user defined task called body() //

   task body();
     super.body; // Sets up the sub-sequencer pointers
     tl_8bit_read_write = rapidio_8bit_read_write_seq::type_id::create("tl_8bit_read_write");
     ll_nwrite = rapidio_ll_nwrite_seq::type_id::create("ll_nwrite");
      ll_nwrite.start(rio_ll_seqcr);
      tl_8bit_read_write.start(rio_tl_seqcr);
     

    // end
   endtask: body

endclass: rapidio_nwrite_sequence 



class rapidio_nwrite_r_sequence extends virtual_sequence_base;

  
   rapidio_ll_nwrite_r_seq ll_nwrite;
 //  rapidio_ll_nread_seq ll_nread;
   rapidio_8bit_read_write_seq tl_8bit_read_write;


  // Register the object in the factory //

   `uvm_object_utils(rapidio_nwrite_r_sequence)

  // Object consruction //

   function new(string name = "rapidio_nwrite_r_sequence");
   super.new(name);
   endfunction

  // Declaration of an user defined task called body() //

   task body();
     super.body; // Sets up the sub-sequencer pointers
     tl_8bit_read_write = rapidio_8bit_read_write_seq::type_id::create("tl_8bit_read_write");
     ll_nwrite = rapidio_ll_nwrite_r_seq::type_id::create("ll_nwrite");
      ll_nwrite.start(rio_ll_seqcr);
      tl_8bit_read_write.start(rio_tl_seqcr);
     

    // end
   endtask: body

endclass: rapidio_nwrite_r_sequence 


class rapidio_response_sequence extends virtual_sequence_base;

  
   rapidio_8bit_read_write_seq tl_8bit_read_write;
   rapidio_ll_response_seq rapidio_ll_reseq;


  // Register the object in the factory //

   `uvm_object_utils(rapidio_response_sequence)

  // Object consruction //

   function new(string name = "rapidio_response_sequence");
   super.new(name);
   endfunction

  // Declaration of an user defined task called body() //

   task body();
     super.body; // Sets up the sub-sequencer pointers
     tl_8bit_read_write = rapidio_8bit_read_write_seq::type_id::create("tl_8bit_read_write");
     rapidio_ll_reseq = rapidio_ll_response_seq::type_id::create("rapidio_ll_reseq");
           rapidio_ll_reseq.start(rio_ll_seqcr);
       tl_8bit_read_write.start(rio_tl_seqcr);

    // end
   endtask: body
//
endclass: rapidio_response_sequence 


class rapidio_err_response_sequence extends virtual_sequence_base;

  
   rapidio_8bit_read_write_seq tl_8bit_read_write;
   rapidio_ll_err_response_seq rapidio_ll_reseq;


  // Register the object in the factory //

   `uvm_object_utils(rapidio_err_response_sequence)

  // Object consruction //

   function new(string name = "rapidio_err_response_sequence");
   super.new(name);
   endfunction

  // Declaration of an user defined task called body() //

   task body();
     super.body; // Sets up the sub-sequencer pointers
     tl_8bit_read_write = rapidio_8bit_read_write_seq::type_id::create("tl_8bit_read_write");
     rapidio_ll_reseq = rapidio_ll_err_response_seq::type_id::create("rapidio_ll_reseq");
     rapidio_ll_reseq.start(rio_ll_seqcr);
     tl_8bit_read_write.start(rio_tl_seqcr);

    // end
   endtask: body
//
endclass: rapidio_err_response_sequence 
